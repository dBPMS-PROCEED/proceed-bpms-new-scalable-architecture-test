import { Config as AuthConfig } from "authlib";

const authConfig: AuthConfig = {
  audience: "https://ms.proceed-labs.org",
  issuer: "https://iosl-proceed-cloud.eu.auth0.com/",
  jwksUri: "https://iosl-proceed-cloud.eu.auth0.com/.well-known/jwks.json",
  algorithms: ["RS256"],
};

export default authConfig;
