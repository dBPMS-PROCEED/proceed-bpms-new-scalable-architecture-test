import { getResourceRouter } from "../lib";

const router = getResourceRouter("role", {
  permissionPrefix: "admin:",
});

export default router;
