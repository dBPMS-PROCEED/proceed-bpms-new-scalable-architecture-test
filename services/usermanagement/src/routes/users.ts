import { authenticated } from "authlib";
import { Request, Response } from "express";
import authConfig from "../authConfig";
import { getResourceRouter } from "../lib";

const router = getResourceRouter("user", {
  permissionPrefix: "admin:",
  // before: (router) => {
  //   router.get(
  //     "/test",
  //     authenticated(undefined, authConfig),
  //     (req: Request, res: Response) => {
  //       res.json("Klappt");
  //     }
  //   );
  // },
  // after: (router) => {
  //   router.get(
  //     "/:resourceId",
  //     authenticated(undefined, authConfig),
  //     (req: Request, res: Response) => {
  //       res.json("Klappt nicht");
  //     }
  //   );
  // },
});

export default router;
