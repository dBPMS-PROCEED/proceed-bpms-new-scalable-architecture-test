import express, { Request, Response } from "express";
import { authenticated, Config as AuthConfig } from "authlib";

import userRouter from "./routes/users";
import roleRouter from "./routes/roles";
import authConfig from "./authConfig";

const app = express();

app.get(
  "/",
  authenticated(undefined, authConfig),
  (req: Request, res: Response) => res.json(req.user)
);

app.use("/users", userRouter);
app.use("/roles", roleRouter);

app.listen(3030, () => {
  console.log("User Management Library now listens on 3030");
});
