import { authenticated } from "authlib";
import { Handler, Request, Response, Router } from "express";

import authConfig from "./authConfig";

interface Options {
  create?: Handler;
  read?: Handler;
  readAll?: Handler;
  update?: Handler;
  updateAll?: Handler;
  delete?: Handler;
  before?: (router: Router) => void;
  after?: (router: Router) => void;
  permissionPrefix?: string;
}

const defaultOptions: Options = {
  create: (req, res) => {
    res.json("create a resource");
  },
  read: (req, res) => {
    res.json("Get the resource " + req.params.resourceId);
  },
  readAll: (req, res) => {
    res.json("Get a list of all resources");
  },
  update: (req, res) => {
    res.json("Update the resource " + req.params.resourceId);
  },
  updateAll: (req, res) => {
    res.json("Update the whole resource " + req.params.resourceId);
  },
  delete: (req, res) => {
    res.json("Delete the resource " + req.params.resourceId);
  },
  permissionPrefix: "",
};

export function getResourceRouter(
  resourcename: string,
  config?: Options
): Router {
  var options: Options = {
    ...defaultOptions,
    ...config,
  };

  const router = Router();

  if (options.before) options.before(router);

  if (options.create) {
    router.post(
      "/",
      authenticated(
        [`${options.permissionPrefix}${resourcename}:create`],
        authConfig
      ),
      (req: Request, res: Response) => {
        res.send("Create a new " + resourcename);
      }
    );
  }

  if (options.readAll) {
    router.get(
      "/",
      authenticated(
        [`${options.permissionPrefix}${resourcename}:read`],
        authConfig
      ),
      options.readAll
    );
  }

  if (options.read) {
    router.get(
      "/:resourceId",
      authenticated(
        [`${options.permissionPrefix}${resourcename}:read`],
        authConfig
      ),
      options.read
    );
  }

  if (options.update) {
    router.patch(
      "/:resourceId",
      authenticated(
        [`${options.permissionPrefix}${resourcename}:update`],
        authConfig
      ),
      options.update
    );
  }

  if (options.updateAll) {
    router.put(
      "/:resourceId",
      authenticated(
        [`${options.permissionPrefix}${resourcename}:update`],
        authConfig
      ),
      options.updateAll
    );
  }

  if (options.delete) {
    router.delete(
      "/:resourceId",
      authenticated(
        [`${options.permissionPrefix}${resourcename}:delete`],
        authConfig
      ),
      options.delete
    );
  }

  if (options.after) options.after(router);

  return router;
}
