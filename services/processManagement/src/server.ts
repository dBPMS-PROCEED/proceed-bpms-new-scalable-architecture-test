import express, { Request, Response } from "express";
import cors from "cors";
import { authenticated, Config as AuthConfig } from "@n3rdc4ptn/authlib";
import { Process, ProcessCollectionApi } from "./api/processdb";

import dotenv from "dotenv"

dotenv.config();

const authConfig: AuthConfig = {
  audience: "https://ms.proceed-labs.org",
  issuer: "https://iosl-proceed-cloud.eu.auth0.com/",
  jwksUri: "https://iosl-proceed-cloud.eu.auth0.com/.well-known/jwks.json",
  algorithms: ["RS256"],
};

const app = express();
app.use(
  cors({
    origin: "*",
  })
);
app.use(express.json());

const SAMPLE_BPMN_CONTENT: string = `<?xml version="1.0" encoding="UTF-8"?>
<bpmn2:definitions xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:bpmn2="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" xsi:schemaLocation="http://www.omg.org/spec/BPMN/20100524/MODEL BPMN20.xsd" id="sample-diagram" targetNamespace="http://bpmn.io/schema/bpmn">
  <bpmn2:process id="Process_1" isExecutable="false">
    <bpmn2:startEvent id="StartEvent_1"/>
  </bpmn2:process>
  <bpmndi:BPMNDiagram id="BPMNDiagram_1">
    <bpmndi:BPMNPlane id="BPMNPlane_1" bpmnElement="Process_1">
      <bpmndi:BPMNShape id="_BPMNShape_StartEvent_2" bpmnElement="StartEvent_1">
        <dc:Bounds height="36.0" width="36.0" x="412.0" y="240.0"/>
      </bpmndi:BPMNShape>
    </bpmndi:BPMNPlane>
  </bpmndi:BPMNDiagram>
</bpmn2:definitions>
`;

app.get(
  "/",
  authenticated(undefined, authConfig),
  (req: Request, res: Response) => {
    res.send("Process Management is running...");
  }
);

app.get(
  "/process",
  authenticated(undefined, authConfig),
  (req: Request, res: Response, next: Function) => {
    let department: string = req.query.department as string;
    var skip: number | undefined = parseInt(req.query.skip as string);
    var limit: number | undefined = parseInt(req.query.limit as string);

    if (isNaN(skip)) skip = undefined;
    if (isNaN(limit)) limit = undefined;

    ProcessCollectionApi.getCollection((err: Error, col: Array<Object>) => {
      if (err) return next(err);
      //TODO: filter based on skip and limit
      res.json(col);
    });
  }
);

app.post(
  "/process",
  authenticated(undefined, authConfig),
  (req: Request, res: Response, next: Function) => {
    let process: Process = req.body;
    //let result = db.postItem(createObject);
    ProcessCollectionApi.createDocument(process, (err: Error, doc: Object) => {
      if (err) return next(err);
      res.status(201).json(doc);
    });
  }
);

app.get(
  "/process/:processId",
  authenticated(undefined, authConfig),
  (req: Request, res: Response, next: Function) => {
    ProcessCollectionApi.getDocument(
      req.params.processId,
      (err: Error, doc: Object) => {
        if (err) return next(err);
        res.json(doc);
      }
    );
  }
);

app.put(
  "/process/:processId",
  authenticated(undefined, authConfig),
  (req: Request, res: Response, next: Function) => {
    let processId: string = req.params.processId;
    let processCreateData: Process = req.body;

    ProcessCollectionApi.replaceDocument(
      processId,
      processCreateData,
      (err: Error, doc: Object) => {
        if (err) return next(err);
        res.sendStatus(200);
      }
    );
  }
);

app.patch(
  "/process/:processId",
  authenticated(undefined, authConfig),
  (req: Request, res: Response, next: Function) => {
    let processId: string = req.params.processId;
    let processCreateData: Process = req.body;

    ProcessCollectionApi.updateDocument(
      processId,
      processCreateData,
      (err: Error, doc: Object) => {
        if (err) return next(err);
        res.sendStatus(200);
      }
    );
  }
);

app.delete(
  "/process/:processId",
  authenticated(undefined, authConfig),
  (req: Request, res: Response, next: Function) => {
    ProcessCollectionApi.removeDocument(
      req.params.processId,
      (err: Error, doc: Object) => {
        if (err) return next(err);
        res.sendStatus(200);
      }
    );
  }
);

module.exports = app;
