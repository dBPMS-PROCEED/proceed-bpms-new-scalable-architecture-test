import {MongolibError} from "mongolib"
import {Request, Response} from "express"

export function errorhandler(err: Error, req: Request, res: Response, next: Function){
	if (err instanceof MongolibError) return res.status(err.status).send(err.msg);
	// unexpected error
	console.log("Unexpected error occured:", err)
	res.status(500).send("Internal server error");
}