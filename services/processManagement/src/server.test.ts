import { Process } from "./api/processdb";
import axios from "axios";

const appUnderTest = require("./server")
const rawRequest = require('supertest')

const API_TOKEN = process.env.PROCEED_API_TOKEN
const EXAMPLE_BPMN = `<?xml version="1.0" encoding="UTF-8"?>
<bpmn2:definitions xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:bpmn2="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" xsi:schemaLocation="http://www.omg.org/spec/BPMN/20100524/MODEL BPMN20.xsd" id="sample-diagram" targetNamespace="http://bpmn.io/schema/bpmn">
  <bpmn2:process id="Process_1" isExecutable="false">
    <bpmn2:startEvent id="StartEvent_1"/>
  </bpmn2:process>
  <bpmndi:BPMNDiagram id="BPMNDiagram_1">
    <bpmndi:BPMNPlane id="BPMNPlane_1" bpmnElement="Process_1">
      <bpmndi:BPMNShape id="_BPMNShape_StartEvent_2" bpmnElement="StartEvent_1">
        <dc:Bounds height="36.0" width="36.0" x="412.0" y="240.0"/>
      </bpmndi:BPMNShape>
    </bpmndi:BPMNPlane>
  </bpmndi:BPMNDiagram>
</bpmn2:definitions>`

let token: string;
let authMethod: string;

async function fetchAuth0Token() {
  const url = "https://iosl-proceed-cloud.eu.auth0.com/oauth/token";
  const body =
    '{"client_id":"RQQLyTg672PuQ9oyWeARNQJZQwBbErXD","client_secret":"HI8U3O476D8OJMjN8gJ3QLSCCvQ5ODCBBuxc7-mLLVdn3uv3B_ulGpV4DQxEXPCs","audience":"https://ms.proceed-labs.org","grant_type":"client_credentials"}';
  const options = { headers: { "content-type": "application/json" } };

  const response = await axios.post(url, body, options);

  return [response.data.access_token, response.data.token_type];
}

describe("Suite", () => {
    beforeEach(async () => {
        // fetch valid auth0 token
        [token, authMethod] = await fetchAuth0Token();
    });

    test("Test / route", async () => {
        const response = await rawRequest(appUnderTest)
        .get("/")
        .set("authorization", `${authMethod} ${token}`);
        expect(response.status).toEqual(200);
        expect(response.text).toEqual("Process Management is running...");
    });
    /*
    let default1processId:string
    let default2processId:string
    
    beforeEach(async () =>  {
        await reset()
        const [process1Id, process2Id] = await addTwoDefaultprocess()
        default1processId = process1Id
        default2processId = process2Id        
    })

    test("Get all process", async () => {
        expect((await getAllprocess()).length).toBe(2)
    })

    test("Get all processes from specific deparment", async () => {
        const res = await getRequest('/process')
            .query({
                department: "nasa"
            })
            .expect(200)

        const processList:CrudObject[] = res.body 
        expect(processList.length).toBe(1)
        expect(processList[0].department).toBe("nasa")
    })

    test("Get process", async () => {
        const process:CrudObject = await getprocess(default1processId)
        expect(process).not.toBeUndefined()
    })

    test("Get process not found", async () => {
        await getRequest('/process/notarealid')
            .expect(404)
    })

    test("Get bpmn link", async () => {
        const process:CrudObject = await getprocess(default1processId)

        const res = await getRequest(`${process.link}`)
            .expect(200)
        const bpmn = res.body

        expect(bpmn).not.toBeUndefined()
    })

    test("Get bpmn link not found", async () => {
        const res = await getRequest(`/bpmn/notarealid`)
            .expect(404)
    })

    test("Delete one process", async () => {
        await deleteprocess(default1processId);

        await getRequest(`/process/${default1processId}`).expect(404)
        expect((await getAllprocess()).length).toBe(1)
    })

    test("Patch one process",async () => {
        const patchPayLoad = {
            "name": "maryyyyy-process",
            "bpmn": EXAMPLE_BPMN,
            "department": "apollo"
          }

        await patchprocess(default1processId, patchPayLoad)

        const process:CrudObject = await getprocess(default1processId)
        expect(process.name).toBe(patchPayLoad.name)
        expect(process.department).toBe(patchPayLoad.department)
    })
    test("Put one process",async () => {
        const putPayLoad = {
            "name": "maryyyyy-process",
            "bpmn": EXAMPLE_BPMN,
            "department": "apollo"
          }

        await putprocess(default1processId, putPayLoad)

        const process:CrudObject = await getprocess(default1processId)
        expect(process.name).toBe(putPayLoad.name)
        expect(process.department).toBe(putPayLoad.department)
    })

    describe("Adding additonal process", () => {

        beforeEach(async () => {
            await addprocess({
                "name": "another-process",
                "bpmn": EXAMPLE_BPMN,
                "department": "siemens"
            })
        })

        test("get new process", async () => {
            expect((await getAllprocess()).length).toBe(3)
        })

    }) */
})

async function reset(){
    await postRequest('/reset')
}

async function addTwoDefaultprocess() {
    let process1:any = await addprocess({
        "name": "marie-process",
        "bpmn": EXAMPLE_BPMN,
        "department": "apollo"
    })
    let process2:any = await addprocess({
        "name": "paul-process",
        "bpmn": EXAMPLE_BPMN,
        "department": "nasa"
    })

    return [process1.id, process2.id]
}

async function addprocess(content:Process):Promise<any>{
    const res = await postRequest("/process")
        .send(content)
        .expect(201)
    
    return res.body
}

async function getAllprocess():Promise<any[]>{
    const res = await getRequest('/process')
        .expect(200)
    
    return res.body
}

async function getprocess(id:string):Promise<any>{
    const res = await getRequest(`/process/${id}`)
        .expect(200)

    return res.body
}

async function deleteprocess(id:string){
    await deleteRequest(`/process/${id}`)
        .expect(202)
}

async function patchprocess(id:string, patchPayload:any){
    await patchRequest(`/process/${id}`, patchPayload)
        .expect(200)
}

async function putprocess(id:string, putPayload:Process){
    await putRequest(`/process/${id}`, putPayload)
        .expect(200)
}

function getRequest(path:string){
    return rawRequest(appUnderTest)
        .get(path)
        .set({
            authorization: `BEARER ${API_TOKEN}`
        })
}
function postRequest(path:string){
    return rawRequest(appUnderTest)
        .post(path)
        .set({
            authorization: `BEARER ${API_TOKEN}`
        })
}
function deleteRequest(path:string){
    return rawRequest(appUnderTest)
        .delete(path)
        .set({
            authorization: `BEARER ${API_TOKEN}`
        })
}

function patchRequest(path:string, patchPayload:Object){
    return rawRequest(appUnderTest)
        .patch(path)
        .set({
            authorization: `BEARER ${API_TOKEN}`
        })
        .send(patchPayload)
}

function putRequest(path:string, putPayLoad:Object){
    return rawRequest(appUnderTest)
        .patch(path)
        .set({
            authorization: `BEARER ${API_TOKEN}`
        })
        .send(putPayLoad)
}