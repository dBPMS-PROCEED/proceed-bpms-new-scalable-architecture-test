import {MongoCollectionApi} from "mongolib";

export interface Process{
    name: string
    bpmn: string
    department: string
}

const processSchema = {
    name: String,
    bpmn: String,
    department: String,
}

export const ProcessCollectionApi = new MongoCollectionApi<Process>("Process", processSchema);