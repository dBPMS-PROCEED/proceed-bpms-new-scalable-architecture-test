const app = require('./server')
const port = 3041;
const mongoose = require('mongoose');
import { errorhandler } from "./error/errorhandler";

// connect to mongodb
var mongoDB = process.env.MONGODB_CONNECTION_URI;
mongoose.connect(mongoDB, { useNewUrlParser: true , useUnifiedTopology: true});
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

// add error handler
app.use(errorhandler);

app.listen(port, () => {
    console.log(`Process management is listening on port ${port}`);
  });
  