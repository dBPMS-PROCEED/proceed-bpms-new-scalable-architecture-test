import express, { Request, Response } from "express";
import cors from "cors";
import { authenticated, Config as AuthConfig } from "authlib";
import { ApiError } from "./error/errorhandler";

import { CreateEngine, IEngine, EngineCollectionApi } from "./api/db";
import { fetchEngineData } from "./api/engine";

const authConfig: AuthConfig = {
  audience: "https://ms.proceed-labs.org",
  issuer: "https://iosl-proceed-cloud.eu.auth0.com/",
  jwksUri: "https://iosl-proceed-cloud.eu.auth0.com/.well-known/jwks.json",
  algorithms: ["RS256"],
};

const app = express();
app.use(
  cors({
    origin: "*",
  })
);
app.use(express.json()); // for parsing application/json


app.get(
  "/",
  authenticated(undefined, authConfig),
  (req: Request, res: Response) => {
    res.send("Engine Management is running...");
  }
);

/**
 * Get list of all registered engine instances
 */
app.get(
  "/engines",
  authenticated(undefined, authConfig),
  (req: Request, res: Response, next: Function) => {
    var skip: number | undefined = parseInt(req.query.skip as string);
    var limit: number | undefined = parseInt(req.query.limit as string);

    if (isNaN(skip)) skip = undefined;
    if (isNaN(limit)) limit = undefined;

    EngineCollectionApi.getCollection((err: Error, col: Array<Object>) => {
      if (err) return next(err);
      //TODO: filter based on skip and limit
      res.json(col);
    });
  }
);

/**
 * Register an engine instance
 */
app.post(
  "/engines",
  authenticated(undefined, authConfig),
  (req: Request, res: Response, next: Function) => {
    if (!("address" in req.body)) return next(ApiError.badRequest());
    let engineRequestData: CreateEngine = req.body;

    //TODO: fetch engine data through provided engine adress
    fetchEngineData(engineRequestData.address);


    // build  db documention for engine
    let engine: IEngine = {
      address: engineRequestData.address,
      alias: engineRequestData.alias,
      name: "<fetched through engine>",
      hostname: "<fetched through engine>",
      description: "<fetched through engine>",
    };

    EngineCollectionApi.createDocument(engine, (err: Error, doc: Object) => {
      if (err) return next(err);
      res.status(201).json(doc);
    });
  }
);

/**
 * Returns registered engine data by engineId
 */
app.get(
  "/engines/:engineId",
  authenticated(undefined, authConfig),
  (req: Request, res: Response, next: Function) => {
    EngineCollectionApi.getDocument(
      req.params.engineId,
      (err: Error, doc: Object) => {
        if (err) return next(err);
        res.json(doc);
      }
    );
  }
);

/**
 * Replace entire engine data of engineId
 */
app.put(
  "/engines/:engineId",
  authenticated(undefined, authConfig),
  (req: Request, res: Response, next: Function) => {
    if (!("address" in req.body)) return next(ApiError.badRequest());
    let engineId: string = req.params.engineId;
    let engineRequestData: CreateEngine = req.body;


    //TODO: fetch engine data through provided engine adress
    fetchEngineData(engineRequestData.address);

    // build  db documention for engine
    let engine: IEngine = {
      address: engineRequestData.address,
      alias: engineRequestData.alias,
      name: "<fetched through engine>",
      hostname: "<fetched through engine>",
      description: "<fetched through engine>",
    };

    EngineCollectionApi.replaceDocument(
      engineId,
      engine,
      (err: Error, doc: Object) => {
        if (err) return next(err);
        res.sendStatus(200);
      }
    );
  }
);

/**
 * Update registered engine address or alias by engineId
 */
app.patch(
  "/engines/:engineId",
  authenticated(undefined, authConfig),
  (req: Request, res: Response, next: Function) => {
    let engineId: string = req.params.engineId;
    let engineRequestData: CreateEngine = req.body;

    //TODO: fetch engine data through provided engine adress
    fetchEngineData(engineRequestData.address);

    // build  db documention for engine
    let engine: IEngine = {
      address: engineRequestData.address,
      alias: engineRequestData.alias,
      name: "<fetched through engine>",
      hostname: "<fetched through engine>",
      description: "<fetched through engine>",
    };

    EngineCollectionApi.updateDocument(
      engineId,
      engine,
      (err: Error, doc: Object) => {
        if (err) return next(err);
        res.sendStatus(200);
      }
    );
  }
);

/**
 * Delete registered engine instance by engineId
 */
app.delete(
  "/engines/:engineId",
  authenticated(undefined, authConfig),
  (req: Request, res: Response, next: Function) => {
    EngineCollectionApi.removeDocument(
      req.params.engineId,
      (err: Error, doc: Object) => {
        if (err) return next(err);
        res.sendStatus(202);
      }
    );
  }
);

module.exports = app;
