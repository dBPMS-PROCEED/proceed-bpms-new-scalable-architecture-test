import {MongoCollectionApi} from "mongolib";

export interface CreateEngine{
  address: string,
  alias: string
}

export interface IEngine{
  address: string,
  alias: string,
  name: string,
  hostname: string,
  description: string
}

// mongoose schema
const engineSchema = {
  address: String,
  alias: String,
  name: String,
  hostname: String,
  description: String
}

export const EngineCollectionApi = new MongoCollectionApi<IEngine>("Engine", engineSchema);