import { errorhandler } from "./error/errorhandler";
const app = require('./server')
const mongoose = require("mongoose");
const port = 3042;

// connect to mongodb
var mongoDB = process.env.MONGODB_CONNECTION_URI;
mongoose.connect(mongoDB, { useNewUrlParser: true , useUnifiedTopology: true});
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

// add error handler
app.use(errorhandler);

app.listen(port, () => {
    console.log(`Engine management is listening on port ${port}`);
  });
  