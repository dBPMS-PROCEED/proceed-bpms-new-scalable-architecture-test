import {MongolibError} from "mongolib"
import {Request, Response} from "express"

export function errorhandler(err: Error, req: Request, res: Response, next: Function){
	if (err instanceof MongolibError) return res.status(err.status).send(err.msg);
	if (err instanceof ApiError) return res.status(err.status).send(err.msg);
	// unexpected error
	console.log("Unexpected error occured:", err)
	res.status(500).send("Internal server error");
}

export class ApiError extends Error{
	status: number;
	msg: string;

	constructor(status: number, msg: string){
		super();
		this.status = status;
		this.msg = msg;
	}

	static resourceNotFound(msg:string = "Not Found") {
		return new ApiError(404, msg);
	}

	static badRequest(msg:string = "Bad Request") {
		return new ApiError(400, msg);
	}
}