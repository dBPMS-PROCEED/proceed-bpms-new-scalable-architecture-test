import { IEngine, CreateEngine } from "./api/db"
import axios from "axios";

const appUnderTest = require("./server")
const rawRequest = require('supertest')

let token: string;
let authMethod: string;

async function fetchAuth0Token() {
  const url = "https://iosl-proceed-cloud.eu.auth0.com/oauth/token";
  const body =
    '{"client_id":"RQQLyTg672PuQ9oyWeARNQJZQwBbErXD","client_secret":"HI8U3O476D8OJMjN8gJ3QLSCCvQ5ODCBBuxc7-mLLVdn3uv3B_ulGpV4DQxEXPCs","audience":"https://ms.proceed-labs.org","grant_type":"client_credentials"}';
  const options = { headers: { "content-type": "application/json" } };

  const response = await axios.post(url, body, options);

  return [response.data.access_token, response.data.token_type];
}


describe("Suite", () => {
    beforeEach(async () => {
        // fetch valid auth0 token
        [token, authMethod] = await fetchAuth0Token();
    });

    test("Test / route", async () => {
        const response = await rawRequest(appUnderTest)
        .get("/")
        .set("authorization", `${authMethod} ${token}`);
        expect(response.status).toEqual(200);
        expect(response.text).toEqual("Engine Management is running...");
    });

    /*
    let default1EngineId:string
    let default2EngineId:string
    
    beforeEach(async () =>  {
        await reset()
        const [engine1Id, engine2Id] = await addTwoDefaultEngines()
        default1EngineId = engine1Id
        default2EngineId = engine2Id        
    })

    test("Get all engines", async () => {
        expect((await getAllEngines()).length).toBe(2)
    })

    test("Get engine", async () => {
        const engine:CrudObject = await getEngine(default1EngineId)
        expect(engine).not.toBeUndefined()
    })

    test("Get engine not found", async () => {
        await getRequest('/engines/notarealid')
            .expect(404)
    })

    test("Delete one engine", async () => {
        await deleteEngine(default1EngineId);

        await getRequest(`/engines/${default1EngineId}`).expect(404)
        expect((await getAllEngines()).length).toBe(1)
    })

    test("Patch one engine",async () => {
        const patchPayLoad = {
            "address": "192.168.0.100",
            "alias": "alice-toaster-new"
          }

        await patchEngine(default1EngineId, patchPayLoad)

        const engine:CrudObject = await getEngine(default1EngineId)
        expect(engine.address).toBe(patchPayLoad.address)
        expect(engine.alias).toBe(patchPayLoad.alias)
    })
    test("Put one engine",async () => {
        const putPayLoad = {
            "address": "192.168.0.100",
            "alias": "alice-toaster-new"
          }

        await putEngine(default1EngineId, putPayLoad)

        const engine:CrudObject = await getEngine(default1EngineId)
        expect(engine.address).toBe(putPayLoad.address)
        expect(engine.alias).toBe(putPayLoad.alias)
    })

    describe("Adding additonal engine", () => {

        beforeEach(async () => {
            await addEngine({
                "address": "192.1.1.1",
                "alias": "alice-toaster"
            })
        })

        test("get new engine", async () => {
            expect((await getAllEngines()).length).toBe(3)
        })

    })
    */
})

async function addTwoDefaultEngines() {
    let engine1:any = await addEngine({
        "address": "192.1.1.1",
        "alias": "alice-toaster"
    })
    let engine2:any = await addEngine({
        "address": "192.2.2.2",
        "alias": "bob-toaster"
    })

    return [engine1._id, engine2._id]
}

async function addEngine(content:CreateEngine):Promise<any>{
    const res = await postRequest("/engines")
        .send(content)
        .expect(201)
    
    return res.body
}

async function getAllEngines():Promise<any[]>{
    const res = await getRequest('/engines')
        .expect(200)
    
    return res.body
}

async function getEngine(id:string):Promise<any>{
    const res = await getRequest(`/engines/${id}`)
        .expect(200)

    return res.body
}

async function deleteEngine(id:string){
    await deleteRequest(`/engines/${id}`)
        .expect(202)
}

async function patchEngine(id:string, patchPayload:any){
    await patchRequest(`/engines/${id}`, patchPayload)
        .expect(200)
}

async function putEngine(id:string, putPayload:CreateEngine){
    await putRequest(`/engines/${id}`, putPayload)
        .expect(200)
}

function getRequest(path:string){
    return rawRequest(appUnderTest)
        .get(path)
        .set({
            authorization: `${authMethod} ${token}`
        })
}
function postRequest(path:string){
    return rawRequest(appUnderTest)
        .post(path)
        .set({
            authorization: `${authMethod} ${token}`
        })
}
function deleteRequest(path:string){
    return rawRequest(appUnderTest)
        .delete(path)
        .set({
            authorization: `${authMethod} ${token}`
        })
}

function patchRequest(path:string, patchPayload:Object){
    return rawRequest(appUnderTest)
        .patch(path)
        .set({
            authorization: `${authMethod} ${token}`
        })
        .send(patchPayload)
}

function putRequest(path:string, putPayLoad:Object){
    return rawRequest(appUnderTest)
        .patch(path)
        .set({
            authorization: `${authMethod} ${token}`
        })
        .send(putPayLoad)
}