import esbuild from "esbuild";

// This file builds the frontend scripts for the puppeteer frontend
// using esbuild

export async function buildScript() {
  esbuild.build({
    entryPoints: ['src/browser/script.js'],
    bundle: true,
    outfile: 'static/script.js',
  }).catch(() => process.exit(1))
}
