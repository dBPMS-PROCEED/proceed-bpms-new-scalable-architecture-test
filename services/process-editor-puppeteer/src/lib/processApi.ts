import fetch from "node-fetch";

var process_uri = process.env.URI_PROCESS_MANAGEMENT;
const accessToken = process.env.AUTH0_ACCESS_TOKEN;


type Process = {
  _id: string;
  name: string;
  bpmn: string;
  department: string;
}

export const processApi = {
  get: async (id: string): Promise<Process> => {

    const response = await fetch(`${process_uri}/process/${id}`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${accessToken}`,
      }
    });

    // Check if accessToken is expired
    if (response.status === 401) throw new Error(`Error while getting process due to expired auth0 access token!`);

    var data = response.json() as Promise<Process>;

    return data
  },
  update: async (id: string, xml: string) => {
    const response = await fetch(`${process_uri}/process/${id}`, {
      method: "PATCH",
      headers: {
        Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        bpmn: xml,
      }),
    })

    // Check if response was successfull (200 and body == Ok)
    if (response.status !== 200) {
      console.error(`Error updating process ${id}`);
    }
  }
}
