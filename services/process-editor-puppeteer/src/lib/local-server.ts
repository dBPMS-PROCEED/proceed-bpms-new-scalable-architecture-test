import express from 'express';



export async function startWebserver() {
  const app = express();

  app.use(express.static('./static'));
  app.set('view engine', 'pug');

  app.get('/', (req, res) => {
    res.render('index');
  });

  app.listen(8888, "localhost", () => {
    console.log("Started local webserver for puppeteer interaction");
  });
}