import { readFile } from 'node:fs/promises';
import fetch from 'node-fetch';

type AuthData = {
  url: string;
  client_id: string,
  client_secret: string,
  audience: string,
  grant_type: string,
}

let accessToken: string | null = null;

async function getAuth0Secret(): Promise<AuthData> {
  const buffer = await readFile('./.config/auth0_secret.json');

  const data = JSON.parse(buffer.toString());

  return data;
}


var options = {
  method: "POST",
  url: "https://iosl-proceed-cloud.eu.auth0.com/oauth/token",
  headers: { "content-type": "application/json" },
  body: "",
};



async function requestNewAccessToken() {
  const secret = await getAuth0Secret();

  const { client_id, client_secret, audience, grant_type } = secret;

  const response = fetch(secret.url, {
    method: "POST",
    headers: {
      "content-type": "application/json",
    },
    body: JSON.stringify({
      client_id,
      client_secret,
      audience,
      grant_type,
    })
  })

  const data: {
    access_token: string;
    token_type: string;
  } = (await response).json() as any;

  accessToken = (await data).access_token;

  return (await data).access_token;
}

/*
requestNewAccessToken();

export {
  accessToken,
  requestNewAccessToken
}
*/