import { registerModeler, activateDistribution, applyExternalEvent, registerSocket } from "bpmn-event-distribution/src/event-distribution";

import { debounce } from "lodash";

var mainModeler;


const fireSavexml = debounce(async () => {
  const bpmn = await mainModeler.saveXML();

  window.saveXML({ xml: bpmn.xml });
}, 1000, {
  maxWait: 2000
});

function bpmnEvent(event) {
  const { processId, command, context } = event.detail;
  
  applyExternalEvent(command, JSON.parse(context));

  // Dispatch savexml event
  fireSavexml();
}

async function xmlFallback(event) {
  console.log(event.detail)
  const { processId, xml } = event.detail;
  
  await mainModeler.importXML(xml);
  
  fireSavexml();
}

export function initCollab(modeler) {
  // Register Modeler in bpmn-event-distribution
  registerModeler(modeler);
  mainModeler = modeler;

  // Activate distribution listener of bpmn-event-distribution
  activateDistribution();

  window.addEventListener("bpmnevent", bpmnEvent);
  window.addEventListener("xmlfallback", xmlFallback);

  // Return a disconnect / unsubscribe function
  return () => {
    window.removeEventListener("bpmnevent", bpmnEvent);
    window.removeEventListener("xmlfallback", xmlFallback);
  }
}