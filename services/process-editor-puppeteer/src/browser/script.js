import BpmnModeler from "bpmn-js/lib/Modeler";
import diagramJsOrigin from "diagram-js-origin";
import CliModule from "bpmn-js-cli"

import { initCollab } from "./collab"

const modeler = new BpmnModeler({
  container: "#app",
  additionalModules: [
    diagramJsOrigin, CliModule
  ]
})


initCollab(modeler);
