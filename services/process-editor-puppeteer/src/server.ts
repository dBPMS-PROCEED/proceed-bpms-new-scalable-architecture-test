import dotenv from "dotenv";
dotenv.config();

import puppeteer from "puppeteer";
import { io } from "socket.io-client";

import { buildScript } from "./lib/build-script";
import { startWebserver } from "./lib/local-server";
import { processApi } from "./lib/processApi";

const socket_backend_url =
  process.env.SOCKET_BACKEND_URL || "http://localhost:3030";

console.log(socket_backend_url);

const socket = io(socket_backend_url, {
  query: {
    type: "puppeteer",
  },
  transports: ["websocket"]
});

const process_pages: { [key: string]: puppeteer.Page } = {};

socket.on("bpmnevent", async (processId, command, context) => {
  const page = process_pages[processId];

  if (page) {
    await page.evaluate(function (processId, command, context) {
      const event = new CustomEvent("bpmnevent", {
        detail: {
          processId,
          command,
          context,
        },
      });

      window.dispatchEvent(event);
    }, processId, command, context);
  }
});

socket.on("xmlfallback", async (processId, xml) => {
  const page = process_pages[processId];

  if (page) {
    await page.evaluate(function (processId, xml) {
      const event = new CustomEvent("xmlfallback", {
        detail: {
          processId,
          xml
        },
      });

      window.dispatchEvent(event);
    }, processId, xml);
  }
});

(async () => {
  const browser = await puppeteer.launch({
    headless: true,
    executablePath: process.env.CHROME_PATH,
    args: [
        "--disable-gpu",
        "--disable-dev-shm-usage",
        "--disable-setuid-sandbox",
        "--no-sandbox",

    ]
  });
  console.log("Puppeteer browser started successfully...");

  await buildScript();
  await startWebserver();

  socket.on("join_process", async (processId) => {
    const page = await browser.newPage();
    await page.goto("http://localhost:8888/");

    process_pages[processId] = page;

    // get process from processManagement and send it back to the socket
    const process = await processApi.get(processId);
    socket.emit("xmlfallback", processId, process.bpmn);
    
    await page.evaluate(function(xml: string, processId: string) {

      document.title = processId

      const event = new CustomEvent("xmlfallback", {
        detail: {
          processId,
          xml
        },
      });

      window.dispatchEvent(event);
    }, process.bpmn, processId);

    await page.exposeFunction("saveXML", async ({ xml }: { xml: string }) => {
      const _ = await processApi.update(processId, xml);
      
      socket.emit("savexml", processId, xml);
    })
  });

  socket.on("leave_process", async (processId) => {
    const page = process_pages[processId];
    if (page) {
      setTimeout(async () => { await page.close() }, 1000);
      delete process_pages[processId];
    }
  });

  // await browser.close();
})();

// listen to / route to check availability
import express from "express" 
const app = express()
const PORT = 3044

app.get("/", (req, res) => res.send("Puppeteer is listening..."))

app.listen(PORT, () => console.log("Puppeteer is listening on " + PORT))