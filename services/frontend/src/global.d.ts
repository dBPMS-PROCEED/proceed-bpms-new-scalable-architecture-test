export declare global {
  export interface Window {
    login: () => void;
    logout: () => void;
  }
}
