import "./style.css";
import authConfig from "./auth_config.json";
import createAuth0Client, { Auth0Client } from "@auth0/auth0-spa-js";

const divGatedContent = document.getElementById("gated-content");
const spanIptAccessToken = document.getElementById("ipt-access-token");
const spanIptIdToken = document.getElementById("ipt-id-token");
const spanIptClaims = document.getElementById("ipt-claims");
const spanIptUserProfile = document.getElementById("ipt-user-profile");
const btnLogin = document.getElementById("btn-login") as HTMLButtonElement;
const btnLogout = document.getElementById("btn-logout") as HTMLButtonElement;

const divUserProfile = document.getElementById("user-profile");

let auth0: Auth0Client | null = null;

// Function to init auth0 client
const configureClient = async () => {
  auth0 = await createAuth0Client({
    ...authConfig,
    cacheLocation: "localstorage",
    audience: "https://ms.proceed-labs.org",
  });
};

// Loads when the window is ready
window.onload = async () => {
  console.log("LOAD");
  await configureClient();

  updateUI();

  const isAuthenticated = await auth0?.isAuthenticated();

  if (isAuthenticated) {
    return;
  }

  const query = window.location.search;
  if (query.includes("code=") && query.includes("state=")) {
    // Process the login state
    await auth0?.handleRedirectCallback();

    updateUI();

    // Use replaceState to redirect the user away and remove the querystring parameters
    window.history.replaceState({}, document.title, "/");
  }
};

// Update UI Function
const updateUI = async () => {
  if (auth0 !== null) {
    const isAuthenticated = await auth0.isAuthenticated();

    if (btnLogin !== null) btnLogin.disabled = isAuthenticated;

    if (btnLogout !== null) btnLogout.disabled = !isAuthenticated;

    if (isAuthenticated) {
      divGatedContent?.classList.remove("hidden");

      let user = await auth0.getUser();

      if (spanIptIdToken !== null)
        spanIptIdToken.innerHTML =
          (await auth0.getIdTokenClaims())?.__raw || "";

      if (spanIptClaims !== null)
        spanIptClaims.innerHTML = JSON.stringify(
          {
            ...(await auth0.getIdTokenClaims()),
            __raw: undefined,
          },
          null,
          2
        );

      if (spanIptAccessToken !== null)
        spanIptAccessToken.innerHTML = await auth0.getTokenSilently();

      if (spanIptUserProfile !== null)
        spanIptUserProfile.textContent = JSON.stringify(user, null, 2);

      const image = document.createElement("img");
      image.src = user?.picture || "";

      const nickname = document.createElement("p");
      nickname.textContent = user?.nickname || "";

      const email = document.createElement("p");
      email.textContent = user?.email || "";

      divUserProfile?.append(image, nickname, email);
    } else {
      divGatedContent?.classList.add("hidden");
    }
  }
};

window.login = async () => {
  console.log(window.location.origin);
  await auth0?.loginWithRedirect({
    redirect_uri: window.location.origin,
  });
};

window.logout = async () => {
  console.log("Hallo");
  console.log(auth0);
  await auth0?.logout({
    returnTo: window.location.origin,
  });
};
