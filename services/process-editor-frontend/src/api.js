import { auth0 } from './auth0'

import { serviceList } from './services';


async function getAuthHeader() {
  const accessToken = await auth0.getTokenSilently();

  return {
    Authorization: `Bearer ${accessToken}`,
  }
}



let process_uri = (await serviceList()).processManagement;


export const process = {
  create: async (name, department) => {

    const sampleXML = `<?xml version="1.0" encoding="UTF-8"?>
<definitions xmlns="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" id="sid-38422fae-e03e-43a3-bef4-bd33b32041b2" targetNamespace="http://bpmn.io/bpmn" exporter="bpmn-js (https://demo.bpmn.io)" exporterVersion="9.3.1">
  <process id="Process_1" isExecutable="false" />
  <bpmndi:BPMNDiagram id="BpmnDiagram_1">
    <bpmndi:BPMNPlane id="BpmnPlane_1" bpmnElement="Process_1" />
  </bpmndi:BPMNDiagram>
</definitions>`

    const response = fetch(`${process_uri}/process`, {
      method: "POST",
      headers: {
        ...await getAuthHeader(),
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        name,
        bpmn: sampleXML,
        department
      })
    })

    if ((await response).status >= 200 && (await response).status < 300) {
      // Success
      return (await response).json()
    }

    return null;
  },
  get: async (id) => {
    const response = fetch(`${process_uri}/process/${id}`, {
      method: "GET",
      headers: {
        ...await getAuthHeader(),
      }
    });

    var data = (await response).json();

    return data
  },
  list: async () => {

    const response = fetch(`${process_uri}/process`, {
      method: "GET",
      headers: {
        ...await getAuthHeader(),
      }
    });

    const data = (await response).json();

    return data;
  }
}


const engine_uri = import.meta.env.VITE_URI_ENGINE_MANAGEMENT;

export const engine = {
  create: async (name) => {

  },
  list: async () => {

    const response = fetch(`${engine_uri}/engines`, {
      method: "GET",
      headers: {
        ...await getAuthHeader(),
      }
    });

    const data = (await response).json();

    return data;
  }
}

const deploy_uri = import.meta.env.VITE_URI_DEPLOYMENT;

export const deployment = {
  deploy: async (process, engine) => {
    const params = new URLSearchParams()
    params.append("processId", process.id)
    params.append("engineId", engine.id)

    const response = fetch(`${deploy_uri}/deploy?${params.toString()}`, {
      method: "GET",
      headers: {
        ...await getAuthHeader(),
      }
    });
  }
}