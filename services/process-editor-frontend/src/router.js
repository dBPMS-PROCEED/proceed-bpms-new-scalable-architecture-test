import { writable } from "svelte/store";


export const current_state = writable("show");
export const current_process = writable(null);