import authConfig from "./auth_config.json"

import createAuth0Client, { Auth0Client } from "@auth0/auth0-spa-js"


export let auth0 = await createAuth0Client({
  ...authConfig,
  cacheLocation: "localstorage",
  audience: "https://ms.proceed-labs.org",
});

const configureClient = async () => {
  
}

export let isAuthenticated = false;


export async function initAuth0() {
  await configureClient();

  const query = window.location.search;
  if (query.includes("code=") && query.includes("state=")) {
    // Process the login state
    await auth0?.handleRedirectCallback();

    // Use replaceState to redirect the user away and remove the querystring parameters
    window.history.replaceState({}, document.title, "/");
  }

  isAuthenticated = await auth0?.isAuthenticated();

  return isAuthenticated;
}


export async function login() {
  await auth0?.loginWithRedirect({
    redirect_uri: window.location.origin
  });
}

export async function logout() {
  await auth0?.logout({
    returnTo: window.location.origin
  })
}
