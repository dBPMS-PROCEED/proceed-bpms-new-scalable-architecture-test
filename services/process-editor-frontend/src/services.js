async function loadServices() {
  if (import.meta.env.VITE_URI_PROCESS_MANAGEMENT && import.meta.env.VITE_URI_SOCKET_BACKEND) {
    return {
      processManagement: import.meta.env.VITE_URI_PROCESS_MANAGEMENT,
      "process-editor-backend": import.meta.env.VITE_URI_SOCKET_BACKEND
    }
  }

  // const response = await fetch("https://storage.googleapis.com/storage/v1/b/cloud-iosl-proceed-static/o/service-register.json?alt=media")
  const response = await fetch("/config/services.json")
  if (response.ok) {
    const data = await response.json()
    return data
  }

  return null
}

let services = null;

export async function serviceList() {
  if (services) {
    return services
  }

  services = await loadServices()
  
  console.log(services);

  return services
}

