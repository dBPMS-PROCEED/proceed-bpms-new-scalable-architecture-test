import { v4 as uuidv4 } from 'uuid';

export const getUUID = () => {
  let uuid = localStorage.getItem('proceedUUID')

  if (uuid === null) {
    uuid = uuidv4()
    localStorage.setItem('proceedUUID', uuid);
  }

  return uuid;
}
