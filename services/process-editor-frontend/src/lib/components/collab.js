import { debounce } from "lodash";
import { io } from "socket.io-client";
import { moveCursor } from "./cursor";
import { getUUID } from "./uuid";

import { registerModeler, activateDistribution, applyExternalEvent, registerSocket } from "bpmn-event-distribution/src/event-distribution";
import { serviceList } from "../../services";


let loadedXml = false;

export async function initCollab(modeler, processId) {
  const services = await serviceList();
  var socket_backend = services["process-editor-backend"]

  const socket = io(socket_backend, {
    query: {
      processId
    },
    transports: ["websocket"]
  });

  console.log("Connected to socket: " + socket_backend);

  // Register Modeler in bpmn-event-distribution
  registerModeler(modeler);
  registerSocket(socket);

  // Activate distribution listener of bpmn-event-distribution
  activateDistribution();

  // Register socket.io events
  socket.on("bpmnevent", (processId, command, context) => {
    if (!loadedXml) return;
    applyExternalEvent(command, JSON.parse(context));
  });
  
  // Fallback when the backend sends the xml
  socket.on("xmlfallback", async (processId, xml) => {
    await modeler.importXML(xml);
    loadedXml = true;
  });


  // Mouse Live event
  const originElement = document.getElementsByClassName('djs-origin-cross')[0]


  let mousemovelistener = debounce(event => {
    const { x: originX, y: originY, width: originWidth, height: originHeight } = originElement.getBoundingClientRect()
  
    const x = event.clientX;
    const y = event.clientY;
  
    // 20 pixel Offset
    const diffX = x - originX - 20;
    const diffY = y - originY - 20;
  
  
    socket.emit("mouseevent", getUUID(), diffX, diffY)
  }, 20)

  document.getElementById('app').addEventListener('mousemove', mousemovelistener)
  

  socket.on("mouseevent", (processId, uuid, x, y) => {
    moveCursor(uuid, x, y)
  });


  window.addEventListener("xml-import", (event) => {
    const { xml } = event.detail;

    modeler.importXML(xml, function(err) {
      if (err) {
        console.error('Error importing XML:', err);
      }

      socket.emit("xmlfallback", xml);
    });
  })

  // Return a disconnect / unsubscribe function
  return () => {
    document.getElementById('app').removeEventListener("mousemove", mousemovelistener);

    socket.disconnect();
  }
}