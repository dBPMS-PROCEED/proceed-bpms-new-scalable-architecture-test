function calcBack(diffX, diffY) {
  const originElement = document.getElementsByClassName('djs-origin-cross')[0]

  const { x: originX, y: originY } = originElement.getBoundingClientRect()

  const x = diffX + originX + 20
  const y = diffY + originY + 20

  return { x, y }
}

function createCursor() {
  const el = document.createElement("div")
  el.style.position = "fixed"
  el.style.height = "10px"
  el.style.width = "10px"
  el.style.backgroundColor = `#${Math.floor(Math.random() * 16777215).toString(16)}`

  el.style.transition = "all 0.5s"

  return el
}


const spawnCursor = (x, y) => {
  const wrapper = document.getElementById("cursorwrapper")
  const cursor = createCursor()

  cursor.style.top = `${y}px`
  cursor.style.left = `${x}px`

  wrapper.appendChild(cursor)

  return cursor
}

export const moveCursor = (uuid, x, y) => {
  var { x, y } = calcBack(x, y)

  let cursor = document.getElementById(`cursor-${uuid}`)
  if (cursor) {
    cursor.style.top = `${y}px`
    cursor.style.left = `${x}px`
    return;
  }

  cursor = spawnCursor(x, y)
  cursor.id = `cursor-${uuid}`
}
