const app = require('./server')
const port = 3043;

app.listen(port, () => {
    console.log(`Instance Monitoring is listening on port ${port}`);
  });
  