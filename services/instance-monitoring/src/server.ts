import express, { Request, Response } from "express";
import cors from "cors";
import { authenticated, Config as AuthConfig } from "authlib";
import { getProcessInstances, getProcessInstanceState } from "./api/requests";

const authConfig: AuthConfig = {
  audience: "https://ms.proceed-labs.org",
  issuer: "https://iosl-proceed-cloud.eu.auth0.com/",
  jwksUri: "https://iosl-proceed-cloud.eu.auth0.com/.well-known/jwks.json",
  algorithms: ["RS256"],
};

const app = express();
app.use(cors({
  origin: "*"
}));
app.use(express.json()); // for parsing application/json

/**
 * Test route
 */
app.get(
  "/",
  authenticated(undefined, authConfig),
  (req: Request, res: Response) => {
    res.send("Instance Monitoring is running...");
  }
);

/**
 * List all instance states of a bpmn process definition
 */
app.get(
  "/processes/:definitionId/instances",
  authenticated(undefined, authConfig),
  (req: Request, res: Response) => {
    return res.json(getProcessInstances(req.params.definitionId,)); 
  }
);


/**
 * Get a specific instance state of a bpmn process
 */
app.get(
  "/processes/:definitionId/instances/:id",
  authenticated(undefined, authConfig),
  (req: Request, res: Response) => {
    return res.json(getProcessInstanceState(req.params.definitionId, req.params.id)); 
  }
);

module.exports = app
