export interface ExportedProcessState {
    processId: string;
    processInstanceId: string;
    globalStartTime: number;
    instanceState: string[];
    tokens: object[];
    variables: object;
    log: object;
}

/**
 * Get all proccess instances states for a given process definition Id 
 * Based on pull approach
 */
export function getProcessInstances(definitionId: string): Array<ExportedProcessState>{
    // case: static deployment
    // ...
    // case: dynamic deployment
    //TODO: get all registered engines through engine management
    //TODO: get state of every engine through Engine Gateway
    //TODO: filter instances based on definitionId
    //TODO: return results
    return [];
}

/**
 * Get specific process instance state  
 * Based on pull approach
 */
export function getProcessInstanceState(definitionId: string, instanceId: string): ExportedProcessState | null{
    // case: static deployment
    // ...
    // case: dynamic deployment
    //TODO: get all registered engines through engine management
    //TODO: get state of every engine through Engine Gateway
    //TODO: determine engine on where process instance with given instanceId is running (handle not found case)
    //TODO: return process instance state,
    return null;

}
