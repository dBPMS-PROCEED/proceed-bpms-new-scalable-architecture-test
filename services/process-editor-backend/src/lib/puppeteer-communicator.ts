import { Server } from "socket.io";
import { PROCESS_ROOM_PREFIX } from "../server";

let ioServer: Server;

export function init(io: Server) {
  ioServer = io;
}

export async function getPuppeteerInstances() {
  const socketsSet = await ioServer.to("puppeteer").allSockets();

  const sockets = Array.from(socketsSet).map(async (socket) => {
    // go through all rooms
    let counter = 0;
    let rooms: string[] = [];
    // add number of clients in each room to the array
    ioServer.sockets.adapter.rooms.forEach((value, key, map) => {
      // rooms contains our socket
      if (Array.from(value).includes(socket)) {
        counter += 1;
        if (key.startsWith(PROCESS_ROOM_PREFIX)) rooms.push(key);
      }

    });

    return {
      id: socket,
      amount: counter - 2,
      rooms: rooms,
    };
  });

  return await Promise.all(sockets);
}

// a function that gets the number of process rooms
// and calculates the optimal allocation of processes to puppeteer instances
function getPuppeteerAllocation() {
  const puppeteerSockets = Array.from(
    ioServer.sockets.adapter.rooms.get("puppeteer") || []
  );
  const puppeteerSocketsWithProcesses = puppeteerSockets.map((socket) => {
    
    const rooms = Array.from(ioServer.sockets.adapter.socketRooms(socket) || [])
    console.log(rooms);
    const processRooms = rooms
      .filter((room) => room.startsWith(PROCESS_ROOM_PREFIX))
      .map((room) => {
        const rooms = Array.from(ioServer.sockets.adapter.rooms.get(room) || []);
        return {
          roomName: room,
          clientAmount: rooms.length,
        };
      })
      .sort((a, b) => a.clientAmount - b.clientAmount)
      .map((room) => room.roomName);

    return {
      id: socket,
      processRooms,
    };
  });

  return puppeteerSocketsWithProcesses;
}


export function reallocatePuppeteers() {
  // This function checks, if the desiredAllocation is equal to the currentAllocation
  function isEqual(curr: number[], des: number[]) {
    let current = [...curr]
    let desired = [...des]

    if (current.length !== desired.length) return false;
    while(current.length) {
      const currentElement = current.shift();
      if (currentElement === undefined) break;
      const needle = desired.indexOf(currentElement)

      if (needle === -1) {
        return false;
      }
      
      delete desired[needle]
    }
    return true;
  }

  console.log("[Alloc] === Start with reallocation ===");

  let puppeteerAllocation = getPuppeteerAllocation();

  // sort puppeteer allocation by length of processRooms
  puppeteerAllocation = puppeteerAllocation.sort((a, b) => b.processRooms.length - a.processRooms.length)

  console.log(puppeteerAllocation);

  const sumOfProcesses = puppeteerAllocation.reduce((acc, puppeteer) => {
    return acc + puppeteer.processRooms.length
  }, 0);
  
  const processesPerPuppeteer = Math.floor(sumOfProcesses / puppeteerAllocation.length);
  const processesRemaining = sumOfProcesses % puppeteerAllocation.length;

  const desiredAllocation = [];
  for (let i = 0; i < puppeteerAllocation.length; i++) {
    desiredAllocation.push(processesPerPuppeteer);
  }
  for (let i = 0; i < processesRemaining; i++) {
    desiredAllocation[i]++;
  }

  console.log(`[Alloc]   Desired Allocation: ${desiredAllocation}`);

  let currentAllocationList = puppeteerAllocation.map(puppeteer => {
    return puppeteer.processRooms.length
  });

  while(!isEqual(currentAllocationList, desiredAllocation)) {
    console.log(`[Alloc]   Current allocation: ${currentAllocationList}`);
    puppeteerAllocation = puppeteerAllocation.sort((a, b) => b.processRooms.length - a.processRooms.length)

    // Get the biggest process room from the first puppeteer
    const smallestProcessRoom = puppeteerAllocation[0].processRooms.shift();

    if (!smallestProcessRoom) { 
      throw new Error("Could not find a process room to reallocate");
    }
    
    // Put the smallestProcess to the last puppeteer
    puppeteerAllocation[puppeteerAllocation.length - 1].processRooms.push(smallestProcessRoom);

    currentAllocationList = puppeteerAllocation.map(puppeteer => puppeteer.processRooms.length);

    const puppeteerToLeave = puppeteerAllocation[0];
    const puppeteerToJoin = puppeteerAllocation[puppeteerAllocation.length - 1];

    // execute the move processRoom to the last puppeteer
    ioServer.to(puppeteerToJoin.id).socketsJoin(smallestProcessRoom);
    setTimeout(() => {
      ioServer.to(puppeteerToLeave.id).socketsLeave(smallestProcessRoom);
    }, 800);
  }
  console.log(`[Alloc]   Current allocation: ${currentAllocationList}`);

  console.log("[Alloc] === Done with reallocation ===")
}