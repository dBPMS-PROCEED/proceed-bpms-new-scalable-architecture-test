import { Server } from "socket.io";
import { createAdapter } from "@socket.io/redis-adapter";
import { createClient, RedisClientType } from "redis";
import dotenv from 'dotenv';
import express, { Request, Response } from "express";
import http from "http";
import { getPuppeteerInstances, reallocatePuppeteers, init } from "./lib/puppeteer-communicator";

import cors from "cors";

dotenv.config();


export const PROCESS_ROOM_PREFIX = "process-";

const PORT = parseInt(process.env.PORT || "3030");

// const authConfig: AuthConfig = {
//   audience: "https://ms.proceed-labs.org",
//   issuer: "https://iosl-proceed-cloud.eu.auth0.com/",
//   jwksUri: "https://iosl-proceed-cloud.eu.auth0.com/.well-known/jwks.json",
//   algorithms: ["RS256"],
// };



const app = express();
const server = http.createServer(app);

app.use(cors({
  origin: "*",
}))

const io = new Server(server, {
  cors: {
    origin: "*",
  },
});

init(io);

// Redis Adapter
var promises: Promise<any>[] = [];
var pubClient: RedisClientType;
var subClient: RedisClientType;

const REDIS_URL = process.env.REDIS_URL || `redis://${process.env.REDIS_USER}:${process.env.REDIS_PASSWORD}@${process.env.REDIS_HOST}:${process.env.REDIS_PORT}`;

// Check if REDIS_URL is set and does not contain undefined
if (REDIS_URL && REDIS_URL.indexOf("undefined") === -1) {
  console.log("Redis URL found. Connect via redis adapter");
  pubClient = createClient({ url: REDIS_URL });
  pubClient.on("error", (err) => {
    console.error(`[REDIS] ${err}`);
  });
  subClient = pubClient.duplicate();

  promises = [pubClient.connect(), subClient.connect()]
}

Promise.all(promises).then(() => {
  if (promises.length > 0) {
    io.adapter(createAdapter(pubClient, subClient));
  }

  io.of("/").adapter.on("create-room", async (room) => {
    console.log(`[Room] created ${room}`);

    // Say puppeteer service, that it should join this room

    // Check if room startswith process-
    if (room.startsWith(PROCESS_ROOM_PREFIX)) {
      // This is a process-room
      // Send message to puppeteer service
      
      const sockets = (await getPuppeteerInstances())
        .sort((a, b) => a.amount - b.amount)
        
      if (sockets.length > 0) {
        // Choose a socket with the least amount of rooms
        const socket = sockets[0].id;
        
        io.to(socket).socketsJoin(room);
        
        io.to(socket).emit("join_process", room.substring(PROCESS_ROOM_PREFIX.length));
      }
    }
  });

  io.of("/").adapter.on("delete-room", async (room) => {
    console.log(`[Room] deleted ${room}`);

    await pubClient.del(room);
  });
  
  io.of("/").adapter.on("join-room", async (room, id) => {
    console.log(`[Room] ${getSocketType(id)} socket ${id} joined room ${room}`);

    if (room.startsWith(PROCESS_ROOM_PREFIX)) {
      // Get socket
      const socket = io.sockets.sockets.get(id);
      if (socket) {
        if (socket.data.normalclient) {
          // This is a normalclient
          // Get current xml from redis client
          const xml = await pubClient.get(room);
          if (xml) {
            // Send processId and xml to client
            socket.emit("xmlfallback", room.substring(PROCESS_ROOM_PREFIX.length), xml);
          }
        }
      }
    }

    if (room === "puppeteer") {
      reallocatePuppeteers();
    }
  });

  io.of("/").adapter.on("leave-room", async (room, id) => {
    console.log(`[Room] ${getSocketType(id)} socket ${id} left room ${room}`);
    const currentSocket = io.sockets.sockets.get(id);

    if(room === "puppeteer") {
      // get all rooms where puppeteer socket was sitting
      let rooms: string[] | undefined = Array.from(currentSocket?.rooms || []);

      // filter puppeteer room out
      rooms = rooms.filter((r) => r != "puppeteer");

      //TODO: check if at least one puppeteer instance exist (or does it await creation)

      // update each process room of leaving puppeteer socket
      console.log("[Alloc] Allocating new puppeteer socket to each room of leaving puppeteer");
      let currentRoom: string;
      for (currentRoom of rooms || []) {
        if (currentRoom.startsWith(PROCESS_ROOM_PREFIX)) {
          // puppeteer leaves current process room
          currentSocket?.leave(currentRoom);

          // Get all puppeteer instances
          const sockets = (await getPuppeteerInstances())
            .filter((s) => s.id != id)
            .sort((a, b) => a.amount - b.amount);

          console.log("[Alloc] We have " + sockets.length + " puppeteer instances");

          if (sockets.length > 0) {
            // Choose a socket with the least amount of rooms
            const socket = sockets[0].id;

            io.to(socket).socketsJoin(currentRoom);
            io.to(socket).emit(
              "join_process",
              currentRoom.substring(PROCESS_ROOM_PREFIX.length)
            );
          } else {
            console.error("[Alloc] No puppeteer sockets available!");
          }
        }
      }
    } else if (currentSocket && !currentSocket.data.normalclient) {
      // Case: Puppeteer is leaving a process room
      // ignore this case
    } else {
      // Case: Normal client is leaving a process room

      // Get all sockets in this room
      const sockets = Array.from(io.sockets.adapter.rooms.get(room) || []);

      if (sockets.length == 1) {
        // This is the last socket in this room
        // Send message to puppeteer service
        io.to(sockets[0]).emit("leave_process", room.substring(PROCESS_ROOM_PREFIX.length));

        // Let socket leave the room
        io.to(sockets[0]).socketsLeave(room);
      }
    }
  });

  io.on("connection", async (socket) => {
    const processId = socket.handshake.query.processId as string;
    const type = socket.handshake.query.type as string;

    // Normal frontend client sends the processId
    if (processId && processId.length > 0) {
      socket.data.puppeteer = false;
      socket.data.normalclient = true

      // create for every process a room
      const roomName = `${PROCESS_ROOM_PREFIX}${processId}`;
      socket.join(roomName);

      const roomSocket = socket.to(roomName);

      // Send back the bpmnevent to all other members of the session
      socket.on("bpmnevent", (command, context) => {
        // console.log("bpmnevent", command, context);
        roomSocket.emit("bpmnevent", processId, command, context);
      });

      socket.on("xmlfallback", (xml) => {
        // console.log("Event:", xml);
        roomSocket.emit("xmlfallback", processId, xml);
      });

      socket.on("mouseevent", (uuid, x, y) => {
        roomSocket.emit("mouseevent", processId, uuid, x, y);
      });

      socket.on("disconnect", () => {
        socket.leave(roomName);
      });
  
    } else if (type == "puppeteer") { // PUPPETEER CLIENT
      // Client is a puppeteer client
      socket.join("puppeteer");

      socket.data.puppeteer = true
      socket.data.normalclient = false;

      socket.on("disconnect", () => {
        socket.leave("puppeteer");
      });

      socket.on("xmlfallback", (processId, xml) => {
        const roomName = `${PROCESS_ROOM_PREFIX}${processId}`;
        socket.join(roomName);

        const roomSocket = socket.to(roomName);
        pubClient.set(roomName, xml);

        roomSocket.emit("xmlfallback", processId, xml);
      });

      socket.on("savexml", (processId, xml) => {
        const roomName = `${PROCESS_ROOM_PREFIX}${processId}`;

        pubClient.set(roomName, xml);
        // console.log(`[XML] save xml for ${roomName} in redis`)
      })

    } else {
      socket.disconnect();
    }
  });

  server.listen(PORT, () => {
    console.log("Editor Backend is now listening on " + PORT);
  });

})

function getSocketType(id: string) {
    // Get socket
    const socket = io.sockets.sockets.get(id);
    if (socket?.data.normalclient  == null ) return "unknown";
    return socket?.data.normalclient ? "client" : "puppeteer";
};