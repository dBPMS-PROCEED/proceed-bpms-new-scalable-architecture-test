const app = require('./server')
const port = 3040;

app.listen(port, () => {
    console.log(`Deployment listening on port ${port}`);
  });
  