// TODO: fix import
export const PROCESS_MANAGEMENT_URL = "http://localhost:3041";
export const ENGINE_MANAGEMENT_URL = "http://localhost:3042";
// import {PROCESS_MANAGEMENT_URL, ENGINE_MANAGEMENT_URL} from "./server"

const rawRequest = require('supertest')
const nock = require('nock')


const appUnderTest = require("./server")

const API_TOKEN = process.env.PROCEED_API_TOKEN

describe("suite", () => {

    let exampleEngineId:string
    let exampleProcessId:string

    beforeEach(() => {
      exampleEngineId = "f9bcbc91-2ff8-46d9-af90-f699a7e2c657"
      exampleProcessId = "5f507549-72a5-457a-bda0-738c4a003de9"
    })

    test("sucessfull deployment", async () => {      
      const scopeProcessManagement = nock(PROCESS_MANAGEMENT_URL)
        .get(`/process/${exampleProcessId}`)
        .reply(200)
      const scopeEngineManagement = nock(ENGINE_MANAGEMENT_URL)
        .get(`/engines/${exampleEngineId}`)
        .reply(200)

      const res = await getRequest(appUnderTest, '/deploy')
        .query({
          processId: exampleProcessId,
          engineId: exampleEngineId
        })
        .expect(200)      
    })

    test("engine not found", async () => {
      const scopeProcessManagement = nock(PROCESS_MANAGEMENT_URL)
        .get(`/process/${exampleProcessId}`)
        .reply(200)
      const scopeEngineManagement = nock(ENGINE_MANAGEMENT_URL)
        .get(`/engines/${exampleEngineId}`)
        .reply(404)

      const res = await getRequest(appUnderTest, '/deploy')
        .query({
          processId: exampleProcessId,
          engineId: exampleEngineId
        })
        .expect(400)    
    })

    test("process not found", async () => {
      const scopeProcessManagement = nock(PROCESS_MANAGEMENT_URL)
        .get(`/process/${exampleProcessId}`)
        .reply(404)
      const scopeEngineManagement = nock(ENGINE_MANAGEMENT_URL)
        .get(`/engines/${exampleEngineId}`)
        .reply(200)

      const res = await getRequest(appUnderTest, '/deploy')
        .query({
          processId: exampleProcessId,
          engineId: exampleEngineId
        })
        .expect(400)    
    })

    test("process and engine not found", async () => {
      const scopeProcessManagement = nock(PROCESS_MANAGEMENT_URL)
        .get(`/process/${exampleProcessId}`)
        .reply(404)
      const scopeEngineManagement = nock(ENGINE_MANAGEMENT_URL)
        .get(`/engines/${exampleEngineId}`)
        .reply(404)

      const res = await getRequest(appUnderTest, '/deploy')
        .query({
          processId: exampleProcessId,
          engineId: exampleEngineId
        })
        .expect(400)    
    })
})


function getRequest(subject:any, path:string){
  return rawRequest(subject)
      .get(path)
      .set({
          authorization: `BEARER ${API_TOKEN}`
      })
}