import express, { Request, Response } from "express";
import { authenticated, checkPerm, Config as AuthConfig } from "authlib";
import cors from 'cors';
const axios = require("axios").default;


export const PROCESS_MANAGEMENT_URL = process.env.PROCESS_MANAGEMENT_URL // "http://localhost:3041"
export const ENGINE_MANAGEMENT_URL = process.env.ENGINE_MANAGEMENT_URL // "http://localhost:3042"

const authConfig: AuthConfig = {
  audience: "https://ms.proceed-labs.org",
  issuer: "https://iosl-proceed-cloud.eu.auth0.com/",
  jwksUri: "https://iosl-proceed-cloud.eu.auth0.com/.well-known/jwks.json",
  algorithms: ["RS256"],
};

const app = express();

app.use(cors({
  origin: "*"
}));


app.get(
  "/",
  authenticated(undefined, authConfig),
  (req: Request, res: Response) => {
    res.send(`Deployment is running. ProcessManagement Url: ${PROCESS_MANAGEMENT_URL} and EngineManagemen Url: ${ENGINE_MANAGEMENT_URL}. You can chanage them by editing the env variables PROCESS_MANAGEMENT_URL and ENGINE_MANAGEMENT_URL`);
  }
);


app.get(
  "/deploy", 
  authenticated(undefined, authConfig),
  async (req: Request, res: Response) => {
    
    const [ getProccessUrl, getEngineUrl ] = parseUrls(
      req.query["processId"] as string,
      req.query["engineId"] as string
    );

    let jwtToken = req.headers['authorization'] 
    if(jwtToken == undefined){
      res.send("JWT token is not valid")
      return
    }

    let [statusProcess, statusEngine]:[number, number] = await evaluateEngineAndProcess(
      jwtToken,
      getProccessUrl,
      getEngineUrl
    );

    console.log("status codes. process: ", statusProcess, "engine: " , statusEngine);
      
    if (statusProcess === 200 && statusEngine === 200) {
      res.status(200).send();
    } else {
      res.status(400).send()
      // TODO: make send possible while testing
      // res.status(200).send(`Processmangement: ${statusProcess}, Enginemangement: ${statusEngine}`)
    }
});

function parseUrls(proccessId: string, engineId: string): [string, string] {
  console.log("processId:", proccessId, " engineId:", engineId);

  const getProccessUrl = PROCESS_MANAGEMENT_URL + "/process/" + proccessId;
  const getEngineUrl = ENGINE_MANAGEMENT_URL + "/engines/" + engineId;

  console.log("process-url: ", getProccessUrl);
  console.log("engine-url: ", getEngineUrl);

  return [ getProccessUrl, getEngineUrl ];
}

async function evaluateEngineAndProcess(jwtToken: string, getProccessUrl: string, getEngineUrl: string): Promise<[number, number]> {
  let statusProcess = await axios
      .get(getProccessUrl, {
          headers: {
              Authorization: jwtToken
          }
      })
      .then(function (response: any) {
          console.log("Got into process")
          return response.status;
      })
      .catch(function (error:any){
          return 404
      });
  
  let statusEngine = await axios
      .get(getEngineUrl, {
          headers: {
              Authorization: jwtToken 
          }
      })
      .then(function (response: any) {
          console.log("Got into engine");
          return response.status
      })
      .catch(function (error:any){
          return 404
      });
      
  
  return [statusProcess, statusEngine]
}


module.exports = app