import { model, Schema, Error, isValidObjectId } from "mongoose";

export class MongolibError extends Error{
  status: number;
  msg: string;

  constructor(status: number, msg: string) {
    super(msg);
    this.status = status;
    this.msg = msg;
  }
  static documentNotFound(msg?: string){
    return new MongolibError(404, msg ? msg : "Not Found");
  }
  static badRequest(msg?: string) {
    return new MongolibError(400, msg ? msg : "Bad Request");

  }
}

export class MongoCollectionApi<IDocument> {
  collectionName: string;
  model: any;

  public constructor(collectionName: string, documentSchema: Object) {
    this.collectionName = collectionName;
    const schema = new Schema<IDocument>(documentSchema);
    this.model = model<IDocument>(collectionName, schema);
  }
  /**
   * Create document and add it to specified collection in mongodb
   * @param {Object} document
   * @param {function} callback
   */
  async createDocument(document: Object, callback: Function): Promise<any> {
    this.model
      .create(document)
      .then((doc: Object) => {
        callback(null, doc);
      })
      .catch((e: Error) => {
        callback(e);
      });
  }

  /**
   *
   * Get all document within specified collection from mongodb
   * @param {function} callback
   */
  async getCollection(callback: Function) {
    this.model
      .find({})
      .then((col: Array<Object>) => {
        callback(null, col);
      })
      .catch((e: Error) => callback(e));
  }

  /**
   * Get document within specified collection from mongodb
   * @param {string} documentId
   * @param {function} callback
   */
  async getDocument(documentId: string, callback: Function) {
    if (!isValidObjectId(documentId)) return callback(MongolibError.badRequest());

    this.model
      .findById(documentId)
      .then((doc: Object) => {
        if (!doc) return callback(MongolibError.documentNotFound())
        callback(null, doc);
      })
      .catch((e: Error) => callback(e));
  }

  /**
   * Replace document entierly in specified collection in mongodb
   * @param {string} documentId
   * @param {Object} changes
   * @param {function} callback
   */
  async replaceDocument(documentId: string, changes: Object, callback: Function) {
    if (!isValidObjectId(documentId)) return callback(MongolibError.badRequest());

    this.model
      .findOneAndReplace({ _id: documentId }, changes, { runValidators: true, new: true })
      .then(function (doc: Object) {
        if (!doc) return callback(MongolibError.documentNotFound())
        callback(null, doc);
      })
      .catch((e: Error) => callback(e));
  }

  /**
   * Update document in specified collection in mongodb
   * @param {string} documentId
   * @param {Object} changes
   * @param {function} callback
   */
  async updateDocument(documentId: string, changes: Object, callback: Function) {
    if (!isValidObjectId(documentId)) return callback(MongolibError.badRequest());

    // get current document in db
    this.model
      .findByIdAndUpdate(documentId, changes, { runValidators: true, new: true })
      .then((doc: Object) => {
        if (!doc) return callback(MongolibError.documentNotFound())
        callback(null, doc);
      })
      .catch((e: Error) => callback(e));
  }

  /**
   * Delete document with :documentId from specified collection in mongodb
   * @param {string} documentId
   * @param {function} callback
   */
  async removeDocument(documentId: string, callback: Function) {
    if (!isValidObjectId(documentId)) return callback(MongolibError.badRequest());

    this.model
      .findByIdAndDelete(documentId)
      .then((doc: Object) => {
        if (!doc) return callback(MongolibError.documentNotFound())
        callback(null, doc);
      })
      .catch((e: Error) => callback(e));
  }
}
