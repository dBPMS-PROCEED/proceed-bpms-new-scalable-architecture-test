"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.checkPerm = exports.authenticated = void 0;
const express_oauth2_jwt_bearer_1 = require("express-oauth2-jwt-bearer");
function getConfigurationFromEnv() {
    var _a;
    return {
        issuer: process.env.AUTH0_ISSUER,
        jwksUri: process.env.AUTH0_JWKS_URI,
        audience: process.env.AUTH0_AUDIENCE,
        algorithms: (_a = process.env.AUTH0_ALGORITHMS) === null || _a === void 0 ? void 0 : _a.split(",").map((t) => t.trim()),
    };
}
const extractClaims = (req, res, next) => {
    const check = (0, express_oauth2_jwt_bearer_1.claimCheck)((claims) => {
        var _a;
        req.user = {
            claims,
            permissions: claims.permissions,
            sub: (_a = claims.sub) !== null && _a !== void 0 ? _a : "",
        };
        return true;
    });
    check(req, res, next);
};
function authenticated(permissions, config) {
    if (config === undefined) {
        config = getConfigurationFromEnv();
    }
    const authCheck = (0, express_oauth2_jwt_bearer_1.auth)({
        audience: config.audience,
        issuer: config.issuer,
        jwksUri: config.jwksUri,
    });
    return [
        authCheck,
        extractClaims,
        permissions
            ? checkPerm(permissions)
            : (req, res, next) => {
                next();
            },
    ];
}
exports.authenticated = authenticated;
function checkPerm(permissions) {
    return (req, res, next) => {
        const userPermissions = req.user.claims.permissions;
        let check;
        if (typeof permissions[0] === "string") {
            check = [permissions];
        }
        else {
            check = permissions;
        }
        const allowed = check.some((perms) => perms.every((perm) => userPermissions.includes(perm)));
        if (allowed) {
            next();
        }
        else {
            res.status(403).json({
                error: "You do not have the required permissions to perform this action.",
            });
        }
    };
}
exports.checkPerm = checkPerm;
