import { Handler } from "express";
import { JWTPayload } from "express-oauth2-jwt-bearer";
export interface User {
    sub: string;
    claims: JWTPayload;
    permissions: string[];
}
declare global {
    namespace Express {
        interface Request {
            user: User;
        }
    }
}
declare module "express-oauth2-jwt-bearer" {
    interface JWTPayload {
        permissions: string[];
    }
}
declare type Permissions = string[] | string[][];
export declare type Config = {
    issuer: string;
    audience: string;
    jwksUri: string;
    algorithms: string[];
};
export declare function authenticated(permissions?: Permissions, config?: Config): Handler[];
export declare function checkPerm(permissions: Permissions): Handler;
export {};
