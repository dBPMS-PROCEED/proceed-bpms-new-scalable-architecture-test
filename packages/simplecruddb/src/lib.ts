import { v4 as uuidv4 } from "uuid";

export interface ItemId{
  id:string
}

export abstract class SimpleCrudDb<CrudObject extends ItemId & GeneratedObject, CreateObject, GeneratedObject>{
  
  protected items:CrudObject[];

  public constructor(items:CrudObject[] = []){
    this.items = items;
  }

  public reset(){
    this.items = []
  }

  public listItems(skip: number = 0, limit: number = 10):CrudObject[]{
    return this.items.slice(skip, skip + limit)
  }

  public getItem(id:string):CrudObject|undefined{
    return this.items.find((item) => item.id === id)
  }

  public deleteItem(id:string):CrudObject|undefined{
    let index:number = this.items.findIndex((item) => item.id === id)
    if (index == -1) return undefined
    return this.items.splice(index, 1)[0]
  }

  public postItem(createObject:CreateObject):CrudObject{
    return this.postItemAndGenerate(this.getGeneratedObject(createObject))
  }
  private postItemAndGenerate(generatedObject:GeneratedObject):CrudObject{
    let item: CrudObject = {id: uuidv4(), ...generatedObject} as CrudObject
    this.items.push(item)

    return item;        
  }

  public updateItem(id:string, createObject:CreateObject): CrudObject|undefined{
    return this.updateItemAndGenerate(id, this.getGeneratedObject(createObject))
  }
  private updateItemAndGenerate(id:string, generatedObject:GeneratedObject):CrudObject|undefined{
    let updatedItem = this.getItem(id)

    if(updatedItem == undefined) return undefined
    updatedItem = {...updatedItem, ...generatedObject}

    this.items.forEach((item, i) => {
        if (item.id === id) {
            this.items[i] = updatedItem as CrudObject
        }
    })

    return updatedItem
  }

  protected abstract getGeneratedObject(createObject:CreateObject):GeneratedObject
}