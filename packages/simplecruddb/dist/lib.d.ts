export interface ItemId {
    id: string;
}
export declare abstract class SimpleCrudDb<CrudObject extends ItemId & GeneratedObject, CreateObject, GeneratedObject> {
    protected items: CrudObject[];
    constructor(items?: CrudObject[]);
    reset(): void;
    listItems(skip?: number, limit?: number): CrudObject[];
    getItem(id: string): CrudObject | undefined;
    deleteItem(id: string): CrudObject | undefined;
    postItem(createObject: CreateObject): CrudObject;
    private postItemAndGenerate;
    updateItem(id: string, createObject: CreateObject): CrudObject | undefined;
    private updateItemAndGenerate;
    protected abstract getGeneratedObject(createObject: CreateObject): GeneratedObject;
}
