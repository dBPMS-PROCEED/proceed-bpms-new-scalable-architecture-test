"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SimpleCrudDb = void 0;
const uuid_1 = require("uuid");
class SimpleCrudDb {
    constructor(items = []) {
        this.items = items;
    }
    reset() {
        this.items = [];
    }
    listItems(skip = 0, limit = 10) {
        return this.items.slice(skip, skip + limit);
    }
    getItem(id) {
        return this.items.find((item) => item.id === id);
    }
    deleteItem(id) {
        let index = this.items.findIndex((item) => item.id === id);
        if (index == -1)
            return undefined;
        return this.items.splice(index, 1)[0];
    }
    postItem(createObject) {
        return this.postItemAndGenerate(this.getGeneratedObject(createObject));
    }
    postItemAndGenerate(generatedObject) {
        let item = Object.assign({ id: (0, uuid_1.v4)() }, generatedObject);
        this.items.push(item);
        return item;
    }
    updateItem(id, createObject) {
        return this.updateItemAndGenerate(id, this.getGeneratedObject(createObject));
    }
    updateItemAndGenerate(id, generatedObject) {
        let updatedItem = this.getItem(id);
        if (updatedItem == undefined)
            return undefined;
        updatedItem = Object.assign(Object.assign({}, updatedItem), generatedObject);
        this.items.forEach((item, i) => {
            if (item.id === id) {
                this.items[i] = updatedItem;
            }
        });
        return updatedItem;
    }
}
exports.SimpleCrudDb = SimpleCrudDb;
