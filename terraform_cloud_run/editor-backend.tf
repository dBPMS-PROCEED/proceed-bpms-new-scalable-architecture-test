# Cloud Run: Process editor backend
resource "google_cloud_run_service" "process-editor-backend" {
  name     = "process-editor-backend"
  location = var.region

  template {
    spec {
      containers {
        image = "europe-west3-docker.pkg.dev/iosl-proceed/iosl-proceed/process-editor-backend:latest"
        ports {
          container_port = 3030
        }
        env {
          name  = "REDIS_URL"
          value = format("redis://%s:%s", google_redis_instance.redis.host, google_redis_instance.redis.port)
        }

        # volume_mount {
        # name       = "auth-secret"
        # mount_path = "/app/.config"
        # read_only  = true
        # }

        # volume {
        # name = "auth-secret"
        # secret {
        # secret_name = kubernetes_secret.auth-secret.metadata[0].name
        # }
        # }
      }
    }

    metadata {
      annotations = {
        # Limit scale up to prevent any cost blow outs!
        "autoscaling.knative.dev/maxScale"        = "5"
        "run.googleapis.com/vpc-access-connector" = google_vpc_access_connector.connector.name
        # all egress from the service should go through the VPC Connector
        "run.googleapis.com/vpc-access-egress" = "private-ranges-only"
      }
    }
  }

  traffic {
    percent         = 100
    latest_revision = true
  }
}