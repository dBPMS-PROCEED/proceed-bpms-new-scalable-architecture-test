data "google_iam_policy" "noauth" {
  binding {
    role = "roles/run.invoker"
    members = [
      "allUsers",
    ]
  }
}

# Allow no authentication for editor backend
resource "google_cloud_run_service_iam_policy" "editor-backend-noauth" {
  location = google_cloud_run_service.process-editor-backend.location
  project  = google_cloud_run_service.process-editor-backend.project
  service  = google_cloud_run_service.process-editor-backend.name

  policy_data = data.google_iam_policy.noauth.policy_data
}

# Allow no authentication for editor puppeteer
resource "google_cloud_run_service_iam_policy" "editor-puppeteer-noauth" {
  location = google_cloud_run_service.process-editor-puppeteer.location
  project  = google_cloud_run_service.process-editor-puppeteer.project
  service  = google_cloud_run_service.process-editor-puppeteer.name

  policy_data = data.google_iam_policy.noauth.policy_data
}

# Allow no authentication for process management
resource "google_cloud_run_service_iam_policy" "procees-management-noauth" {
  location = google_cloud_run_service.process-management.location
  project  = google_cloud_run_service.process-management.project
  service  = google_cloud_run_service.process-management.name

  policy_data = data.google_iam_policy.noauth.policy_data
}