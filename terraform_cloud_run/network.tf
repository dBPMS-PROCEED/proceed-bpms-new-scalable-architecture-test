# Serverless VPC Access Connector
resource "google_vpc_access_connector" "connector" {
  name          = "vpc-con"
  ip_cidr_range = "10.8.0.0/28"
  network       = "default"
  region        = var.region
}

# VPC
# resource "google_compute_network" "redis-network" {
# name = "redis-test-network"
# auto_create_subnetworks = false
# }