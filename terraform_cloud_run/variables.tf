variable "project_id" {
  description = "The project ID to host the cluster in"
}

variable "region" {
  description = "The region to host the container"
  default     = "europe-west3"
}

variable "zone" {
  type        = string
  description = "The zone to host the container"
  default     = "europe-west3-a"
}

variable "credentials_file" {
  description = "The credentials file of the service account"
}

variable "MONGODB_CONNECTION_URI" {
  description = "The uri to the atlas mongdb"
}

variable "puppeteer_auth0_token" {
  type        = string
  description = "The auth0 token provided to puppetter pod"
}