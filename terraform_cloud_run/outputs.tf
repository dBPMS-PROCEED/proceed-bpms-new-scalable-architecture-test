
output "redis_url" {
  description = "REDIS_URL"
  value       = format("redis://%s:%s", google_redis_instance.redis.host, google_redis_instance.redis.port)
}

output "SOCKET_BACKEND_URL" {
  description = "SOCKET_BACKEND_URL"
  value       = replace(google_cloud_run_service.process-editor-backend.status[0].url, "http:", "ws:")
}