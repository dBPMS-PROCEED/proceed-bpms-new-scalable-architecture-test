resource "google_cloud_run_service" "process-editor-puppeteer" {
  name       = "process-editor-puppeteer"
  location   = var.region
  depends_on = [google_cloud_run_service_iam_policy.editor-backend-noauth]
  //depends_on = [google_secret_manager_secret.secret, google_secret_manager_secret_iam_member.secret-access]

  template {
    spec {
      containers {
        image = "europe-west3-docker.pkg.dev/iosl-proceed/iosl-proceed/process-editor-puppeteer:latest"
        env {
          name = "URI_PROCESS_MANAGEMENT"
          # value: http://process-management:3041
          value = google_cloud_run_service.process-management.status[0].url
        }
        env {
          name = "SOCKET_BACKEND_URL"
          # adjust schema to websocket
          value = replace(google_cloud_run_service.process-editor-backend.status[0].url, "http:", "ws:")
        }
        env {
          name = "AUTH0_ACCESS_TOKEN"
          value = var.puppeteer_auth0_token
        }
        //env {
        //name  = "CHROME_PATH"
        //value = "/usr/bin/chromium-browser"
        //}
        ports {
          container_port = 3044
        }
      }
    }
  }

  traffic {
    percent         = 100
    latest_revision = true
  }
}