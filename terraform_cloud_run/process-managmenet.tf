# Cloud Run: Process management
resource "google_cloud_run_service" "process-management" {
  name     = "process-management"
  location = var.region

  template {
    spec {
      containers {
        image = "europe-west3-docker.pkg.dev/iosl-proceed/iosl-proceed/process-management:latest"
        env {
          name  = "MONGODB_CONNECTION_URI"
          value = var.MONGODB_CONNECTION_URI
        }
        ports {
          container_port = 3041
        }
      }
    }
  }

  traffic {
    percent         = 100
    latest_revision = true
  }
}