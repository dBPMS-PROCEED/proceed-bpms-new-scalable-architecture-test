variable "project_id" {
  description = "The project ID to host the cluster in"
}

variable "region" {
  description = "The region to host the cluster in"
  default     = "europe-west3"
}

variable "zone" {
  type        = string
  description = "The zone to host the cluster in"
  default     = "europe-west3-a"
}

variable "cluster_name" {
  type        = string
  description = "The name of the cluster"
  default     = "proceed-dev"
}

variable "lb_ip" {
  type        = string
  description = "The IP address of the kong load balancer"
  default     = "127.0.0.1"
}

variable "maximum_pod_instances" {
  type        = number
  description = "The maximum number of pod instances"
  default     = 10
}

variable "puppeteer_auth0_token" {
  type        = string
  description = "The auth0 token provided to puppetter pod"
}