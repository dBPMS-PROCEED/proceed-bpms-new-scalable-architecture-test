data "google_client_config" "default" {}

provider "kubernetes" {
  host                   = "https://${module.gke.endpoint}"
  token                  = data.google_client_config.default.access_token
  cluster_ca_certificate = base64decode(module.gke.ca_certificate)
}

provider "helm" {
  kubernetes {
    host                   = "https://${module.gke.endpoint}"
    token                  = data.google_client_config.default.access_token
    cluster_ca_certificate = base64decode(module.gke.ca_certificate)
  }
}

module "gke" {
  source = "terraform-google-modules/kubernetes-engine/google"

  project_id = var.project_id

  name = var.cluster_name

  regional = false
  region   = var.region
  zones    = [var.zone]

  network           = google_compute_network.main.name
  subnetwork        = google_compute_subnetwork.main.name
  ip_range_pods     = google_compute_subnetwork.main.secondary_ip_range[0].range_name
  ip_range_services = google_compute_subnetwork.main.secondary_ip_range[1].range_name

  remove_default_node_pool = true

  enable_vertical_pod_autoscaling = true

  node_pools = [
    {
      name         = "pool-1"
      machine_type = "e2-small"
      preemptible  = true


      min_count = 0
      max_count = 5

      spot         = false
      disk_size_gb = 32

      auto_repair  = true
      auto_upgrade = true

      image_type = "COS_CONTAINERD"
    }
  ]
}


module "proceed-kubernetes" {
  source = "./kubernetes"

  lb_ip = var.lb_ip

  maximum_pod_instances = var.maximum_pod_instances

  puppeteer_auth0_token = var.puppeteer_auth0_token
}