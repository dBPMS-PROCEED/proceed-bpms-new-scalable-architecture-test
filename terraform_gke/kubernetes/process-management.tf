resource "kubernetes_deployment" "process-management" {
  metadata {
    name = "process-management"

    namespace = kubernetes_namespace.proceed.metadata[0].name

    labels = {
      app = "process-management"
    }    
  }

  spec {
    replicas = 1
    selector {
      match_labels = {
        app = "process-management"
      }
    }
    template {
      metadata {
        labels = {
          app = "process-management"
        }
      }
      spec {
        container {
          name = "process-management"
          image = "ioslproceed/service-process-management:latest"
          
          port {
            container_port = 3041
          }

          resources {
            requests = {
              cpu    = "50m"
              memory = "128Mi"
            }
            limits = {
              cpu    = "100m"
              memory = "512Mi"
            }
          }

          env {
            name = "MONGODB_CONNECTION_URI"
            value_from {
              secret_key_ref {
                name = "db-secret"
                key = "MONGODB_CONNECTION_URI"
                optional = false
              }
            }
          }
        }
      }
    }
  }
}


resource "kubernetes_service" "process-management" {
  metadata {
    name = "process-management"
    namespace = kubernetes_namespace.proceed.metadata[0].name
  }

  spec {
    selector = {
      app = "process-management"
    }

    port {
      port = 3041
      target_port = kubernetes_deployment.process-management.spec[0].template[0].spec[0].container[0].port[0].container_port
    }
  }
}

resource "kubernetes_ingress_v1" "process-management-ingress" {
  metadata {
    name = "process-management-ingress"
    namespace = kubernetes_namespace.proceed.metadata[0].name

    annotations = {
      "kubernetes.io/ingress.class" = "kong"
      "konghq.com/strip-path" = "true"
    }
  }

  spec {
    rule {
      # host = "process-management.${data.kubernetes_service.kong.status[0].load_balancer[0].ingress[0].ip}.nip.io"
      host = "api.${var.lb_ip}.nip.io"
      http {
        path {
          path = "/process"
          backend {
            service {
              name = kubernetes_service.process-management.metadata[0].name
              port {
                number = kubernetes_service.process-management.spec[0].port[0].target_port
              }
            }
          }
        }
      }
    }
  }
}

resource "kubernetes_horizontal_pod_autoscaler_v2beta2" "process-management" {
  metadata {
    name = "process-management"
    namespace = kubernetes_namespace.proceed.metadata[0].name
  }

  spec {
    scale_target_ref {
      api_version = "apps/v1"
      kind = "Deployment"
      name = kubernetes_deployment.process-management.metadata[0].name
    }

    min_replicas = 1
    max_replicas = var.maximum_pod_instances

    metric {
      type = "Resource"
      resource {
        name = "cpu"
        target {
          type = "Utilization"
          average_utilization = 80
        }
      }
    }
    
    metric {
      type = "Resource"
      resource {
        name = "memory"
        target {
          type = "Utilization"
          average_utilization = 80
        }
      }
    }
  }
}