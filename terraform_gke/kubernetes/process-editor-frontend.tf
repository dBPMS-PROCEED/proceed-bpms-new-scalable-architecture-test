resource "kubernetes_deployment" "process-editor-frontend" {
  depends_on = [
    kubernetes_config_map.service-config
  ]
  metadata {
    name = "process-editor-frontend"

    namespace = kubernetes_namespace.proceed.metadata[0].name

    labels = {
      app = "process-editor-frontend"
    }    
  }

  spec {
    replicas = 1
    selector {
      match_labels = {
        app = "process-editor-frontend"
      }
    }
    template {
      metadata {
        labels = {
          app = "process-editor-frontend"
        }
      }
      spec {
        container {
          name = "process-editor-frontend"
          image = "ioslproceed/process-editor-frontend:latest"
          
          port {
            container_port = 80
          }

          resources {
            requests = {
              cpu    = "1m"
              memory = "32Mi"
            }
            limits = {
              cpu    = "10m"
              memory = "64Mi"
            }
          }

          # Just that the frontend restarts on lb_ip change
          env {
            name = "LB_IP"
            value = var.lb_ip
          }

          volume_mount {
            name = "service-config"
            mount_path = "/usr/share/caddy/config"
            read_only = true
          }
        }

        volume {
          name = "service-config"
          config_map {
            name = kubernetes_config_map.service-config.metadata[0].name
            items {
              key = "services"
              path = "services.json"
            }
          }
        }
      }
    }
  }
}


resource "kubernetes_service" "process-editor-frontend" {
  metadata {
    name = "process-editor-frontend"
    namespace = kubernetes_namespace.proceed.metadata[0].name
  }

  spec {
    selector = {
      app = "process-editor-frontend"
    }

    port {
      port = 80
      target_port = kubernetes_deployment.process-editor-frontend.spec[0].template[0].spec[0].container[0].port[0].container_port
    }
  }
}

resource "kubernetes_ingress_v1" "process-editor-frontend-ingress" {
  metadata {
    name = "process-editor-frontend-ingress"
    namespace = kubernetes_namespace.proceed.metadata[0].name

    annotations = {
      "kubernetes.io/ingress.class" = "kong"
      "konghq.com/strip-path" = "true"
    }
  }

  spec {
    
    rule {
      # host = "process-editor-frontend.${data.kubernetes_service.kong.status[0].load_balancer[0].ingress[0].ip}.nip.io"
      host = "proceed.${var.lb_ip}.nip.io"
      http {
        path {
          path = "/"
          backend {
            service {
              name = kubernetes_service.process-editor-frontend.metadata[0].name
              port {
                number = kubernetes_service.process-editor-frontend.spec[0].port[0].target_port  
              }
            }
            
          }
        }
      }
    }
  }
}


resource "kubernetes_horizontal_pod_autoscaler_v2beta2" "process-editor-frontend" {
  metadata {
    name = "process-editor-frontend"
    namespace = kubernetes_namespace.proceed.metadata[0].name
  }

  spec {
    scale_target_ref {
      api_version = "apps/v1"
      kind = "Deployment"
      name = kubernetes_deployment.process-editor-frontend.metadata[0].name
    }

    min_replicas = 1
    max_replicas = var.maximum_pod_instances
    
    metric {
      type = "Resource"
      resource {
        name = "cpu"
        target {
          type = "Utilization"
          average_utilization = 80
        }
      }
    }
    
    metric {
      type = "Resource"
      resource {
        name = "memory"
        target {
          type = "Utilization"
          average_utilization = 80
        }
      }
    }
  }
}