resource "helm_release" "kong" {
  name = "kong"
  chart = "kong/kong"
  namespace = "kong"

  create_namespace = true
}


data "kubernetes_service" "kong" {
  depends_on = [
    helm_release.kong
  ]
  metadata {
    name = "kong-kong-proxy"
  }
}