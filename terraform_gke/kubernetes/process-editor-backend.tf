resource "kubernetes_deployment" "process-editor-backend" {
  metadata {
    name = "process-editor-backend"

    namespace = kubernetes_namespace.proceed.metadata[0].name

    labels = {
      app = "process-editor-backend"
    }    
  }

  spec {
    replicas = 1
    selector {
      match_labels = {
        app = "process-editor-backend"
      }
    }
    template {
      metadata {
        labels = {
          app = "process-editor-backend"
        }
      }
      spec {
        container {
          name = "process-editor-backend"
          image = "ioslproceed/process-editor-backend:latest"
          
          port {
            container_port = 3030
          }

          resources {
            requests = {
              cpu    = "100m"
              memory = "128Mi"
            }
            limits = {
              cpu    = "250m"
              memory = "512Mi"
            }
          }

          env {
            name = "REDIS_USER"
            value = "default"
          }

          env {
            name = "REDIS_PASSWORD"
            value_from {
              secret_key_ref {
                name = "redis"
                key = "redis-password"
                optional = false
              }
            }
          }

          env {
            name = "REDIS_HOST"
            value = "redis-master"
          }

          env {
            name = "REDIS_PORT"
            value = "6379"
          }
        }
      }
    }
  }
}


resource "kubernetes_service" "process-editor-backend" {
  metadata {
    name = "process-editor-backend"
    namespace = kubernetes_namespace.proceed.metadata[0].name
  }

  spec {
    selector = {
      app = "process-editor-backend"
    }

    port {
      port = 3030
      target_port = kubernetes_deployment.process-editor-backend.spec[0].template[0].spec[0].container[0].port[0].container_port
    }
  }
}

resource "kubernetes_ingress_v1" "process-editor-backend-ingress" {
  metadata {
    name = "process-editor-backend-ingress"
    namespace = kubernetes_namespace.proceed.metadata[0].name

    annotations = {
      "kubernetes.io/ingress.class" = "kong"
      "konghq.com/strip-path" = "true"
    }
  }

  spec {
    rule {
      # host = "process-editor-backend.${data.kubernetes_service.kong.status[0].load_balancer[0].ingress[0].ip}.nip.io"
      host = "process-editor-backend.${var.lb_ip}.nip.io"
      http {
        path {
          path = "/"
          backend {
            service {
              name = kubernetes_service.process-editor-backend.metadata[0].name

              port {
                number = kubernetes_service.process-editor-backend.spec[0].port[0].target_port
              }
            }
          }
        }
      }
    }
  }
}


resource "kubernetes_horizontal_pod_autoscaler_v2beta2" "process-editor-backend" {
  metadata {
    name = "process-editor-backend"
    namespace = kubernetes_namespace.proceed.metadata[0].name
  }

  spec {
    scale_target_ref {
      api_version = "apps/v1"
      kind = "Deployment"
      name = kubernetes_deployment.process-editor-backend.metadata[0].name
    }

    min_replicas = 1
    max_replicas = var.maximum_pod_instances
    
    metric {
      type = "Resource"
      resource {
        name = "cpu"
        target {
          type = "Utilization"
          average_utilization = 80
        }
      }
    }
    
    metric {
      type = "Resource"
      resource {
        name = "memory"
        target {
          type = "Utilization"
          average_utilization = 80
        }
      }
    }
    //metric {
      //type = "Resource"
      //resource {
        //name = "cpu"
        //target {
          //type = "cpu"
          //average_utilization = 80
        //}
      //}
    //}

    //metric {
      //metric {
        //type = "Resource"
        //resource = "cpu"
        //target = "average"
        //average_utilization = 80
      //}
      //metric {
        //type = "Resource"
        //resource = "memory"
        //target = "average"
        //average_utilization = 80
      //}
    //}
  }
}