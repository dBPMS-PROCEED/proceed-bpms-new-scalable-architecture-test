resource "kubernetes_deployment" "process-editor-puppeteer" {
  metadata {
    name = "process-editor-puppeteer"

    namespace = kubernetes_namespace.proceed.metadata[0].name

    labels = {
      app = "process-editor-puppeteer"
    }    
  }

  spec {
    replicas = 1
    selector {
      match_labels = {
        app = "process-editor-puppeteer"
      }
    }
    template {
      metadata {
        labels = {
          app = "process-editor-puppeteer"
        }
      }
      spec {
        container {
          name = "process-editor-puppeteer"
          image = "ioslproceed/process-editor-puppeteer:latest"

          resources {
            requests = {
              cpu    = "200m"
              memory = "512Mi"
            }
            limits = {
              cpu    = "500m"
              memory = "1Gi"
            }
          }

          env {
            name = "URI_PROCESS_MANAGEMENT"
            value = "http://${kubernetes_service.process-management.metadata[0].name}:${kubernetes_service.process-management.spec[0].port[0].port}"
          }

          env {
            name = "SOCKET_BACKEND_URL"
            value = "ws://${kubernetes_service.process-editor-backend.metadata[0].name}:${kubernetes_service.process-editor-backend.spec[0].port[0].port}"
          }

          env {
            name = "AUTH0_ACCESS_TOKEN"
            value = var.puppeteer_auth0_token
          }
        }
      }
    }
  }
}

resource "kubernetes_horizontal_pod_autoscaler_v2beta2" "process-editor-puppeteer" {
  metadata {
    name = "process-editor-puppeteer"
    namespace = kubernetes_namespace.proceed.metadata[0].name
  }

  spec {
    scale_target_ref {
      api_version = "apps/v1"
      kind = "Deployment"
      name = kubernetes_deployment.process-editor-puppeteer.metadata[0].name
    }

    min_replicas = 1
    max_replicas = var.maximum_pod_instances
    metric {
      type = "Resource"
      resource {
        name = "cpu"
        target {
          type = "Utilization"
          average_utilization = 80
        }
      }
    }
    
    metric {
      type = "Resource"
      resource {
        name = "memory"
        target {
          type = "Utilization"
          average_utilization = 80
        }
      }
    }
  }
}
