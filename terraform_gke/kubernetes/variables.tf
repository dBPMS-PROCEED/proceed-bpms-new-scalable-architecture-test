
variable "lb_ip" {
  type = string
  description = "The IP address of the kong load balancer"
  default = "127.0.0.1"
}


variable "maximum_pod_instances" {
  type = number
  description = "The maximum number of pod instances"
  default = 30
}

variable "puppeteer_auth0_token" {
  type = string
  description = "The auth0 token provided to puppetter pod" 
}