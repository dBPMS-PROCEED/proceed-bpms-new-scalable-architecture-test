resource "kubernetes_config_map" "service-config" {
  depends_on = [
    kubernetes_ingress_v1.process-editor-backend-ingress,
    kubernetes_ingress_v1.process-management-ingress
  ]

  metadata {
    name = "service-config"
    namespace = kubernetes_namespace.proceed.metadata[0].name
  }

  data = {
    "services" = <<EOF
    {
      "processManagement": "https://${kubernetes_ingress_v1.process-management-ingress.spec[0].rule[0].host}${kubernetes_ingress_v1.process-management-ingress.spec[0].rule[0].http[0].path[0].path}",
      "process-editor-backend": "wss://${kubernetes_ingress_v1.process-editor-backend-ingress.spec[0].rule[0].host}"
    }
  EOF
  }
}