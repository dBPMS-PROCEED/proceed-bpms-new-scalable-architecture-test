resource "helm_release" "redis" {
  name = "redis"
  chart = "bitnami/redis"
  namespace = kubernetes_namespace.proceed.metadata[0].name

  values = [
    "${file("./kubernetes/redis-values.yml")}"
  ]
}


data "kubernetes_service" "redis" {
  depends_on = [
    helm_release.redis
  ]
  metadata {
    name = "redis-master"
  }
}