resource "kubernetes_secret" "db-secret" {
  metadata {
    name = "db-secret"
    namespace = kubernetes_namespace.proceed.metadata[0].name
  }

  data = {
    "MONGODB_CONNECTION_URI" = "${file("db-secret.txt")}"
  }  
}