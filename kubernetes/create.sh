#!/bin/bash


# Connect to the google cloud kubernetes cluster
# You need to be sign in using gcloud before running this script
gcloud container clusters get-credentials proceed-dev --zone europe-west3-a --project iosl-proceed



# Create a namespace
kubectl apply -f namespace.yml


# Create kong
./kong/upgrade.sh


# Create redis db
cd redis && ./upgrade.sh && cd ..

